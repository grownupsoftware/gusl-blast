package gusl.blast.graph;

import gusl.blast.exception.BlastException;
import gusl.blast.graph.core.BlastGraphImpl;
import gusl.blast.graph.core.GraphCollection;
import gusl.blast.graph.core.example.model.EventDO;
import gusl.blast.graph.core.example.model.MarketDO;
import gusl.blast.graph.core.example.model.RunnerDO;
import gusl.blast.graph.core.utils.CollectionTraversor;
import gusl.blast.server.BlastServerImpl;
import gusl.core.json.JsonUtils;
import gusl.core.utils.IdGenerator;
import lombok.CustomLog;
import org.junit.Test;

import java.util.*;

import static gusl.core.json.ObjectMapperFactory.prettyPrint;
import static gusl.core.utils.Utils.safeStream;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.fail;

@CustomLog
public class GraphTest {
    private long barCounter = 0;

    protected final Random theRandom = new Random();

    @Test
    public void test_graphDefinition() {
        try {
            IdGenerator.setForTesting();
            logger.info("---here we go---");

            BlastServerImpl blastServer = new BlastServerImpl();
            BlastGraphImpl blastGraph = new BlastGraphImpl();
            blastGraph.setBlastServer(blastServer);

            GraphCollection<EventDO, String> events = blastGraph.<EventDO, String>createCollection()
                    .name("events")
                    .keyField("id")
                    .dataClass(EventDO.class)
                    .excludeField("markets")
                    .build();

            GraphCollection<MarketDO, String> markets = blastGraph.<MarketDO, String>createCollection()
                    .name("markets")
                    .keyField("id")
                    .dataClass(MarketDO.class)
                    //.onlyFields("name")
                    .excludeField("runners")
                    .build();

            // GraphCollection<RunnerDO, String> runners =
            // create runners collection
            final GraphCollection<RunnerDO, String> runners = blastGraph.<RunnerDO, String>createCollection()
                    .name("runners")
                    .keyField("id")
                    .dataClass(RunnerDO.class)
                    //.onlyFields("name")
                    .excludeField("prices")
                    .build();

            // event 1-* markets.eventId
            markets.linkedTo(events).using("eventId");
            runners.linkedTo(markets).using("marketId");

            addTestData(events, markets, runners);

//            logger.info("After Create Collections: {}", getCollectionSummary(blastGraph));
//            logger.info("Schema: {}", SchemaUtils.dumpSchema(blastGraph.getGraph()));
//
//            logger.info("Root Data: {}", prettyPrint(blastGraph.getRootData(null)));
            logger.info("Data key: {}: Data:{}", "101", prettyPrint(blastGraph.getData("101", null)));
//            logger.info("Data Collection: {}: Data:{}", "events", prettyPrint(blastGraph.getCollectionData("events", null)));
//            blastGraph.bubbleUp("102");

        } catch (Throwable ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        } finally {
            // BlastUtils.sleep(1000);
        }

    }

    @Test
    public void test_traversal() {
        try {
            IdGenerator.setForTesting();
            List<Map<String, Object>> testData = createIntegerTestList("id", 10);

            Map<String, Object> record = CollectionTraversor.findRecord("root/id:5", testData);
            logger.info("foo: {}", record.get("foo"));
            assertTrue("foo should match", record.get("foo").equals("foo:5"));

            logger.info("record: {}", JsonUtils.prettyPrint(record));

        } catch (Throwable ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        } finally {
            // BlastUtils.sleep(1000);
        }

    }

    private void createData(int numberEvents, int numMarketsPerEvent, int numRunnersPerMarket,
                            GraphCollection<EventDO, String> eventCollection,
                            GraphCollection<MarketDO, String> marketCollection,
                            GraphCollection<RunnerDO, String> runnerCollection
    ) throws BlastException {
        for (int eventCnt = 0; eventCnt < numberEvents; eventCnt++) {
            String eventId = IdGenerator.generateUniqueNodeIdAsString();
            eventCollection.add(createEvent(eventId, "event " + eventId, "open"));
            for (int marketCnt = 0; marketCnt < numMarketsPerEvent; marketCnt++) {
                String marketId = IdGenerator.generateUniqueNodeIdAsString();
                marketCollection.add(createMarket(marketId, eventId, "market " + marketId, "open"));
                for (int runnerCnt = 0; runnerCnt < numRunnersPerMarket; runnerCnt++) {
                    String runnerId = IdGenerator.generateUniqueNodeIdAsString();
                    runnerCollection.add(createRunner(runnerId, eventId, marketId, "runner " + runnerId, "open"));
                }
            }
        }
    }

    private void addTestData(GraphCollection<EventDO, String> events, GraphCollection<MarketDO, String> markets, GraphCollection<RunnerDO, String> runners) throws BlastException {
        // ------------------------------ create 1st event
        String eventId = "101";
        events.add(createEvent(eventId, "event " + eventId, "open"));

        // add 1st market to event
        String marketId = "101_1";
        markets.add(createMarket(marketId, eventId, "market " + marketId, "open"));

        // add 2nd market
        marketId = "101_2";
        markets.add(createMarket(marketId, eventId, "market " + marketId, "open"));

        // ------------------------------ create 2nd event
        eventId = "102";
        events.add(createEvent(eventId, "event " + eventId, "open"));

        // add 1st market
        marketId = "102_1";
        markets.add(createMarket(marketId, eventId, "market " + marketId, "open"));

        // add runner to market
        String runnerId = "102_1_1";
        RunnerDO lastRunnerDo = createRunner(runnerId, eventId, marketId, "runner " + runnerId, "open");
        runners.add(lastRunnerDo);

//            createData(1, 1, 1, events, markets, runners);

    }

    private EventDO createEvent(String id, String name, String status) {
        EventDO eventDo = new EventDO();
        eventDo.setId(id);
        eventDo.setName(name);
        eventDo.setStatus(status);
        return eventDo;
    }

    private MarketDO createMarket(String id, String eventId, String name, String status) {
        MarketDO marketDo = new MarketDO();
        marketDo.setId(id);
        marketDo.setEventId(eventId);
        marketDo.setName(name);
        marketDo.setStatus(status);
        return marketDo;
    }

    private RunnerDO createRunner(String id, String eventId, String marketId, String name, String status) {
        RunnerDO runnerDo = new RunnerDO();
        runnerDo.setId(id);
        runnerDo.setEventId(eventId);
        runnerDo.setMarketId(marketId);
        runnerDo.setName(name);
        runnerDo.setStatus(status);
        return runnerDo;
    }

    private String getCollectionSummary(BlastGraphImpl blastGraph) throws BlastException {
        StringBuilder builder = new StringBuilder();
        builder.append("\n").append("Collections:").append("\n");
        safeStream(blastGraph.getCollections()).forEach(collection -> {
            builder.append("\t").append(collection.toString()).append("\n");
        });
        return builder.toString();
    }

    private List<Map<String, Object>> createStringTestList(String keyField, int numRecords) {
        List<Map<String, Object>> list = new ArrayList<>();
        for (int x = 0; x < numRecords; x++) {
            byte[] bytes = new byte[5];
            theRandom.nextBytes(bytes);
            list.add(createObject(keyField, String.valueOf(bytes)));
        }
        return list;
    }

    private List<Map<String, Object>> createIntegerTestList(String keyField, int numRecords) {
        List<Map<String, Object>> list = new ArrayList<>();
        for (int x = 0; x < numRecords; x++) {
            list.add(createObject(keyField, x));
        }
        return list;
    }

    private List<Map<String, Object>> createComplexIntegerTestList(String keyField, int numRecords) {
        List<Map<String, Object>> list = new ArrayList<>();
        for (int x = 0; x < numRecords; x++) {
            list.add(createComplexObject(keyField, x));
        }
        return list;
    }

    private Map<String, Object> createObject(String keyField, Object keyValue) {
        Map<String, Object> record = new HashMap<>();

        record.put(keyField, keyValue);
        record.put("foo", "foo:" + keyValue);
        record.put("bar", "bar:" + barCounter++);
        return record;
    }

    private Map<String, Object> createComplexObject(String keyField, Object keyValue) {
        Map<String, Object> record = new HashMap<>();

        record.put(keyField, keyValue);
        record.put("foo", "foo:" + keyValue);
        record.put("bar", createIntegerTestList("id", 10));
        return record;
    }

}
