package gusl.blast.graph;

import gusl.blast.exception.BlastException;
import gusl.blast.graph.core.example.model.RunnerDO;
import gusl.blast.graph.core.utils.CollectionTraversor;
import lombok.CustomLog;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static gusl.core.json.JsonUtils.prettyPrint;
import static org.junit.Assert.*;

@CustomLog
public class GraphAddTest extends AbstractGraphTest {

    @Test
    public void test_add_to_object() {
        logger.info("-- Testing Add");
        try {

            Map<String, Object> collection = new HashMap<>();
            getClient().attach("markets/id:101", collection).get(1, TimeUnit.SECONDS);
            //logger.info("collection: {}", prettyPrint(collection));

            assertTrue("collection should not be empty", !collection.isEmpty());

            createRunner("999");

            Map<String, Object> record = CollectionTraversor.findRecord("runners/id:999", collection);
            logger.info("new record: {}", prettyPrint(record));
            assertNotNull("Should find newly added runner", record);
            assertTrue("name should have changed", record.get("name").equals("new name"));
            assertTrue("status should have changed", record.get("status").equals("open"));

        } catch (InterruptedException | ExecutionException | TimeoutException | BlastException ex) {
            logger.error("error", ex);
            fail(ex.getMessage());
        }
    }

    private void createRunner(String runnerId) throws InterruptedException, BlastException, ExecutionException, TimeoutException {
        // add a runner
        RunnerDO runnerDo = createRunner("100", "101", runnerId, "new name", "open");
        CompletableFuture<Boolean> future = getClient().add("runners", runnerDo);

        Boolean result = future.get(1, TimeUnit.SECONDS);

        logger.info("Record added: {}", result);

        assertTrue("record should have been added", result);

    }
}
