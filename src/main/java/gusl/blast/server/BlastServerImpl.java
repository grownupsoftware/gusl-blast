/*
 * Grownup Software Limited.
 */
package gusl.blast.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.blast.Controllable;
import gusl.blast.blaster.Blaster;
import gusl.blast.blaster.BlasterImpl;
import gusl.blast.client.BlastServerClient;
import gusl.blast.client.ClientManager;
import gusl.blast.client.ClientManagerModule;
import gusl.blast.eventbus.BlastEventBusImpl;
import gusl.blast.exception.BlastException;
import gusl.blast.message.BroadcastEvent;
import gusl.blast.module.BlastModule;
import gusl.blast.server.config.BlastProperties;
import gusl.blast.server.config.BlastPropertiesImpl;
import gusl.core.eventbus.LmEventBus;
import lombok.CustomLog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Blast Server.
 * <p>
 * A blast server will have an event bus, blasters and client manager.
 *
 * @author dhudson - Mar 16, 2017 - 1:32:09 PM
 */
@CustomLog
public class BlastServerImpl implements BlastServer {

    private BlastProperties properties;
    private final ClientManager clientManager;
    private boolean isStarted = false;
    private final LmEventBus eventBus;

    private final Blaster blaster;
    private final HashMap<String, BlastModule> modules;
    private ObjectMapper objectMapper;

    public BlastServerImpl() {
        this(new BlastEventBusImpl());
    }

    public BlastServerImpl(LmEventBus eventBus) {
        modules = new LinkedHashMap<>();

        this.eventBus = eventBus;
        if (eventBus instanceof BlastModule) {
            installModule((BlastModule) eventBus);
        }

        clientManager = new ClientManagerModule();
        installModule(clientManager);

        blaster = new BlasterImpl();
        installModule(blaster);
    }

    @Override
    public void startup() throws BlastException {
        if (!isStarted) {
            logger.debug("Starting Blast Server ...");

            // If this has come from the builder, this will not happen.
            if (properties == null) {
                logger.warn("Properties not set, using defaults");
                // Using default properties
                properties = new BlastPropertiesImpl();
            }

            // Modules can depend on modules, so lets get the current list
            List<BlastModule> currentModules = new ArrayList<>(modules.values());
            // Call config on the modules
            for (BlastModule module : currentModules) {
                registerModule(module);
            }

            // Need to be after the configure, as this is where events gets registered.
            eventBus.post(new ServerStartingEvent(this));

            currentModules = new ArrayList<>(modules.values());
            for (BlastModule module : currentModules) {
                if (module instanceof Controllable) {
                    ((Controllable) module).startup();
                }
            }

            isStarted = true;
            eventBus.post(new ServerStartedEvent(this));
        }
    }

    @Override
    public void shutdown() {
        if (isStarted) {
            logger.debug("Stopping Blast Server ...");

            eventBus.post(new ServerClosingEvent(this));

            for (BlastModule module : modules.values()) {
                if (module instanceof Controllable) {
                    ((Controllable) module).shutdown();
                }
            }

            isStarted = false;
        }
    }

    @Override
    public void setProperties(BlastProperties properties) {
        this.properties = properties;
    }

    @Override
    public BlastProperties getProperties() {
        return properties;
    }

    @Override
    public LmEventBus getEventBus() {
        return eventBus;
    }

    @Override
    public BlastModule requireModule(String ID, Class<? extends BlastModule> module) throws BlastException {
        BlastModule existing = modules.get(ID);
        if (existing != null) {
            return existing;
        }

        // Lets create a new one
        try {
            existing = module.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new BlastException("Unable to create new instance of Module " + ID);
        }

        registerModule(existing);
        return existing;
    }

    @Override
    public BlastServer registerEventListener(Object eventListener) {
        eventBus.register(eventListener);
        return this;
    }

    @Override
    public final BlastServer installModule(BlastModule module) {
        modules.put(module.getModuleID(), module);
        return this;
    }

    @Override
    public final BlastServer registerModule(BlastModule module) throws BlastException {
        module.configure(this);
        installModule(module);
        return this;
    }

    @Override
    public void blast(String message) {
        eventBus.post(new BroadcastEvent(message));
    }

    @Override
    public List<BlastServerClient> getClients() {
        return clientManager.getClients();
    }

    @Override
    public BlastServerClient getClientByID(String id) {
        return clientManager.getClientByID(id);
    }

    @Override
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public void setObjectMapper(ObjectMapper mapper) {
        objectMapper = mapper;
    }

    @Override
    public BlastModule getModule(String moduleID) {
        return modules.get(moduleID);
    }

    @Override
    public void postEvent(Object event) {
        eventBus.post(event);
    }

    @Override
    public boolean hasModule(String ID) {
        return modules.containsKey(ID);
    }
}
