/*
 * Grownup Software Limited.
 */
package gusl.blast.server.config;

import gusl.core.annotations.DocProperty;

/**
 * EndPoint POJO.
 *
 * @author dhudson - Mar 15, 2017 - 4:38:30 PM
 */
public class Endpoint {

    @DocProperty(description = "Name given to the end point", defaultValue = "Blast Endpoint")
    public String name = "Blast Endpoint";

    @DocProperty(description = "Port address to listen on", defaultValue = "8080")
    public int port = 8080;

    @DocProperty(description = "Total number of awaiting connections", defaultValue = "100")
    public int backlog = 100;

    public Endpoint() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getBacklog() {
        return backlog;
    }

    public void setBacklog(int backlog) {
        this.backlog = backlog;
    }

    @Override
    public String toString() {
        return "Endpoint{" + "name=" + name + ", port=" + port + ", backlog=" + backlog + '}';
    }

}
