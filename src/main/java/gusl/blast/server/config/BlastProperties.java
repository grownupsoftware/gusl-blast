/*
 * Grownup Software Limited.
 */
package gusl.blast.server.config;

/**
 * Loaded Config Interface.
 *
 * @author dhudson - Mar 15, 2017 - 4:56:39 PM
 */
public interface BlastProperties {

    int getEventBusThreadSize();

    int getBlastersThreadSize();

    Endpoint getEndpoint();

    int getInputBufferSize();

    int getOutputBufferSize();

    boolean isKeepAlive();

    boolean isReuseAddress();

    boolean isNoDelay();

    int getSoLinger();

}
