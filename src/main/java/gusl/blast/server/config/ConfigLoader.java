/*
 * Grownup Software Limited.
 */
package gusl.blast.server.config;

import com.fasterxml.jackson.databind.JavaType;
import gusl.core.config.ConfigUtils;
import gusl.core.utils.IOUtils;
import lombok.CustomLog;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.ParameterizedType;

/**
 * Base Config Loader.
 *
 * @param <E> Config POJO
 * @author dhudson - Mar 15, 2017 - 3:55:49 PM
 */
@CustomLog
public abstract class ConfigLoader<E extends Object> {

    public ConfigLoader() {
    }

    /**
     * load a Node config file - file must be on class path
     *
     * @param fileName the name of the file
     * @return the populated property POJO or null
     */
    public E loadConfig(String fileName) {

        Class clazz = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        JavaType type = ConfigUtils.getObjectMapper().getTypeFactory().constructSimpleType(clazz, null);
        E properties = null;

        try (InputStream is = IOUtils.getResourceAsStream(fileName, getClass().getClassLoader())) {
            properties = ConfigUtils.getObjectMapper().readValue(is, type);
        } catch (IOException ex) {
            logger.error("Failed to read config file: {}", fileName, ex);
        }

        return properties;
    }

}
