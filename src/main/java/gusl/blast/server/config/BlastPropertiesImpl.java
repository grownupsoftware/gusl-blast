/*
 * Grownup Software Limited.
 */
package gusl.blast.server.config;

import gusl.core.annotations.DocProperty;
import lombok.CustomLog;

/**
 * Server Properties.
 * <p>
 * Hangs off the server.
 *
 * @author dhudson - Mar 15, 2017 - 4:56:52 PM
 */
@CustomLog
public class BlastPropertiesImpl extends AbstractProperties<ServerConfig> implements BlastProperties, BaseProperties {

    @DocProperty(description = "The System Property Key to look for a config file", defaultValue = "blast-server.config")
    private static final String PROPERTY_KEY = "blast-server.config";

    @DocProperty(description = "The default file name for the blast config", defaultValue = "blast-server.json")
    private static final String DEFAULT_NAME = "blast-server.json";

    private ServerConfig serverConfig;

    public BlastPropertiesImpl() {
        // Sensible defaults
        serverConfig = new ServerConfig();
    }

    public void load() {
        serverConfig = loadConfig(getPropertyName());
    }

    @Override
    protected String getDefaultPropertyName() {
        return DEFAULT_NAME;
    }

    @Override
    protected String getPropertySystemKey() {
        return PROPERTY_KEY;
    }

    @Override
    public int getBlastersThreadSize() {
        return serverConfig.getBlastersThreadPoolSize();
    }

    @Override
    public Endpoint getEndpoint() {
        return serverConfig.getEndpoint();
    }

    @Override
    public int getInputBufferSize() {
        return serverConfig.getSocketConditioning().getInputBufferSize();
    }

    @Override
    public int getOutputBufferSize() {
        return serverConfig.getSocketConditioning().getOutputBufferSize();
    }

    @Override
    public boolean isKeepAlive() {
        return serverConfig.getSocketConditioning().isKeepAlive();
    }

    @Override
    public boolean isReuseAddress() {
        return serverConfig.getSocketConditioning().isReuseAddress();
    }

    @Override
    public boolean isNoDelay() {
        return serverConfig.getSocketConditioning().isNoDelay();
    }

    @Override
    public int getSoLinger() {
        return serverConfig.getSocketConditioning().getSoLinger();
    }

    @Override
    public String toString() {
        return "BlastPropertiesImpl{" + serverConfig + '}';
    }

    @Override
    public int getEventBusThreadSize() {
        return serverConfig.getEventBusThreadPoolSize();
    }

}
