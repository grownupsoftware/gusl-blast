/*
 * Grownup Software Limited.
 */
package gusl.blast.server.config;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gusl.core.annotations.DocProperty;
import gusl.core.config.ByteNumberDeserializer;

/**
 * Socket Conditioning POJO.
 *
 * @author dhudson - Mar 15, 2017 - 4:33:23 PM
 */
public class SocketConditioning {

    @DocProperty(description = "Socket input buffer size", defaultValue = "8k")
    @JsonDeserialize(using = ByteNumberDeserializer.class)
    public int inputBufferSize = 8192;

    @DocProperty(description = "Socket output buffer size", defaultValue = "8k")
    @JsonDeserialize(using = ByteNumberDeserializer.class)
    public int outputBufferSize = 8192;

    @DocProperty(description = "Sets SO_KEEPALIVE", defaultValue = "true")
    public boolean keepAlive = true;

    @DocProperty(description = "Sets SO_REUSEADDR", defaultValue = "true")
    public boolean reuseAddress = true;

    @DocProperty(description = "Sets TCP_NO_DELAY", defaultValue = "true")
    public boolean noDelay = true;

    @DocProperty(description = "Socket Linger Option, 0 to turn off", defaultValue = "0")
    public int soLinger = 0;

    public SocketConditioning() {
    }

    public int getInputBufferSize() {
        return inputBufferSize;
    }

    public void setInputBufferSize(int inputBufferSize) {
        this.inputBufferSize = inputBufferSize;
    }

    public int getOutputBufferSize() {
        return outputBufferSize;
    }

    public void setOutputBufferSize(int outputBufferSize) {
        this.outputBufferSize = outputBufferSize;
    }

    public boolean isKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(boolean keepAlive) {
        this.keepAlive = keepAlive;
    }

    public boolean isReuseAddress() {
        return reuseAddress;
    }

    public void setReuseAddress(boolean reuseAddress) {
        this.reuseAddress = reuseAddress;
    }

    public boolean isNoDelay() {
        return noDelay;
    }

    public void setNoDelay(boolean noDelay) {
        this.noDelay = noDelay;
    }

    public int getSoLinger() {
        return soLinger;
    }

    public void setSoLinger(int soLinger) {
        this.soLinger = soLinger;
    }

    @Override
    public String toString() {
        return "SocketConditioning{" + "inputBufferSize=" + inputBufferSize + ", outputBufferSize=" + outputBufferSize + ", keepAlive=" + keepAlive + ", reuseAddress=" + reuseAddress + ", noDelay=" + noDelay + ", soLinger=" + soLinger + '}';
    }

}
