/*
 * Grownup Software Limited.
 */
package gusl.blast.server.config;

import gusl.core.exceptions.GUSLException;

/**
 * @author dhudson - Mar 16, 2017 - 1:05:17 PM
 */
public interface BaseProperties {

    public long getAsMillis(String time) throws GUSLException;

    public long getAsBytes(String bytes) throws GUSLException;

    public int getAsThreads(String threads) throws GUSLException;
}
