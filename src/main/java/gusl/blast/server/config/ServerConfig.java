/*
 * Grownup Software Limited.
 */
package gusl.blast.server.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gusl.core.annotations.DocProperty;
import gusl.core.config.CoreCountDeserializer;
import gusl.core.utils.Platform;

/**
 * POJO of the JSON config.
 *
 * @author dhudson - Mar 15, 2017 - 3:52:16 PM
 */
@JsonInclude(Include.NON_DEFAULT)
public class ServerConfig {

    public static final String DEFAULT_CONFIG_NAME = "ServerConfig.json";

    // Create new ones of these as they contain sensible defaults
    @DocProperty(description = "Blast Endpoint Pojo")
    public Endpoint endpoint = new Endpoint();

    @DocProperty(description = "Socket Conditioning Pojo")
    public SocketConditioning socketConditioning = new SocketConditioning();

    // Number of cores by default
    @DocProperty(description = "Number of threads for the event bus", defaultValue = "2")
    @JsonDeserialize(using = CoreCountDeserializer.class)
    public int eventBusThreadPoolSize = 2;

    @DocProperty(description = "Number of threads for the blasters", defaultValue = "Number of available cores")
    @JsonDeserialize(using = CoreCountDeserializer.class)
    public int blastersThreadPoolSize = Platform.getNumberOfCores();

    public ServerConfig() {
    }

    public int getEventBusThreadPoolSize() {
        return eventBusThreadPoolSize;
    }

    public void setEventBusThreadPoolSize(int size) {
        eventBusThreadPoolSize = size;
    }

    public int getBlastersThreadPoolSize() {
        return blastersThreadPoolSize;
    }

    public void setBlastersThreadPoolSize(int size) {
        this.blastersThreadPoolSize = size;
    }

    public Endpoint getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(Endpoint endpoint) {
        this.endpoint = endpoint;
    }

    public SocketConditioning getSocketConditioning() {
        return socketConditioning;
    }

    public void setSocketConditioning(SocketConditioning socketConditioning) {
        this.socketConditioning = socketConditioning;
    }

    @Override
    public String toString() {
        return "ServerConfig{" + "endpoint=" + endpoint + ", socketConditioning=" + socketConditioning + ", eventBusThreadPoolSize=" + eventBusThreadPoolSize + ", blastersThreadPoolSize=" + blastersThreadPoolSize + '}';
    }

}
