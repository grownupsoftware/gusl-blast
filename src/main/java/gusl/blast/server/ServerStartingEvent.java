/*
 * Grownup Software Limited.
 */
package gusl.blast.server;

import gusl.core.annotations.DocEvent;

import static gusl.core.annotations.DocEvent.EventSource.SERVER;

/**
 * Server Starting Event.
 *
 * @author dhudson - Mar 25, 2017 - 7:59:19 AM
 */
@DocEvent(source = SERVER, description = "This event is produced by the BlastServer, after the installed modules have been configured, but before the modules have been started.")
public class ServerStartingEvent extends ServerEvent {

    public ServerStartingEvent(BlastServer server) {
        super(server);
    }

}
