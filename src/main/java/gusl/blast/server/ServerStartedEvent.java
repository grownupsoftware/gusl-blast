/*
 * Grownup Software Limited.
 */
package gusl.blast.server;

import gusl.core.annotations.DocEvent;

import static gusl.core.annotations.DocEvent.EventSource.SERVER;

/**
 * Server Started Event.
 *
 * @author dhudson - Mar 25, 2017 - 7:59:31 AM
 */
@DocEvent(source = SERVER, description = "This event is produced by the BlastServer, after the installed modules have been started.")
public class ServerStartedEvent extends ServerEvent {

    public ServerStartedEvent(BlastServer server) {
        super(server);
    }

}
