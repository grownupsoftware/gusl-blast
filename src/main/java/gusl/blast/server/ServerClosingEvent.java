/*
 * Grownup Software Limited.
 */

package gusl.blast.server;

import gusl.core.annotations.DocEvent;

import static gusl.core.annotations.DocEvent.EventSource.SERVER;

/**
 * Server Closing Event.
 *
 * @author dhudson - Mar 25, 2017 - 7:59:43 AM
 */
@DocEvent(source = SERVER, description = "This event is produced by the BlastServer, before the installed modules are shutdown.")
public class ServerClosingEvent extends ServerEvent {

    public ServerClosingEvent(BlastServer server) {
        super(server);
    }

}
