/*
 * Grownup Software Limited.
 */
package gusl.blast.server;

import gusl.blast.Controllable;
import gusl.blast.client.BlastServerClient;
import gusl.blast.exception.BlastException;
import gusl.blast.module.BlastModule;
import gusl.blast.server.config.BlastProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.eventbus.LmEventBus;

import java.util.List;

/**
 * Created on Mar 15, 2017, 10:36:42 AM
 *
 * @author dhudson
 */
public interface BlastServer extends Controllable {

    // Properties    
    void setProperties(BlastProperties properties);

    BlastProperties getProperties();

    // Object Mapper
    ObjectMapper getObjectMapper();

    // Event Bus    
    LmEventBus getEventBus();

    BlastServer registerEventListener(Object eventListener);

    /**
     * Send a message to each connected bytes.
     * <p>
     * This could be Stringifed JSON or just text. The client will have to know
     * how to handle it.
     *
     * @param message
     */
    void blast(String message);

    void postEvent(Object event);

    // Modules ..
    BlastServer registerModule(BlastModule module) throws BlastException;

    BlastServer installModule(BlastModule module);

    BlastModule getModule(String moduleID);

    boolean hasModule(String ID);

    BlastModule requireModule(String ID, Class<? extends BlastModule> module) throws BlastException;

    // Client Manager Module
    List<BlastServerClient> getClients();

    BlastServerClient getClientByID(String id);
}
