/*
 * Grownup Software Limited.
 */
package gusl.blast.eventbus;

import gusl.blast.Controllable;
import gusl.blast.exception.BlastException;
import gusl.blast.server.BlastServer;
import gusl.core.eventbus.LmEventBusImpl;

/**
 * @author dhudson - Apr 10, 2017 - 3:06:02 PM
 */
public class BlastEventBusImpl extends LmEventBusImpl implements BlastEventBus, Controllable {

    public static final String EVENTBUS_MODULE_ID = "co.gusl.event.bus";

    @Override
    public void configure(BlastServer server) throws BlastException {
        configure(server.getProperties().getEventBusThreadSize());
    }

    @Override
    public String getModuleID() {
        return EVENTBUS_MODULE_ID;
    }

    @Override
    public void startup() throws BlastException {
    }

}
