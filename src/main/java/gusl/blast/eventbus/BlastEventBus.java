/*
 * Grownup Software Limited.
 */
package gusl.blast.eventbus;

import gusl.blast.Controllable;
import gusl.blast.module.BlastModule;
import gusl.core.eventbus.LmEventBus;

/**
 * @author dhudson - Mar 23, 2017 - 8:49:32 AM
 */
public interface BlastEventBus extends LmEventBus, Controllable, BlastModule {

}
