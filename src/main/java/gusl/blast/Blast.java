/*
 * Grownup Software Limited.
 */
package gusl.blast;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.blast.module.BlastModule;
import gusl.blast.server.BlastServer;
import gusl.blast.server.BlastServerImpl;
import gusl.blast.server.config.BlastProperties;
import gusl.blast.server.config.BlastPropertiesImpl;
import gusl.core.eventbus.LmEventBus;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.logging.GUSLLogger;
import lombok.CustomLog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dhudson - Mar 20, 2017 - 2:01:04 PM
 */
@CustomLog
public class Blast {

    // Hold Blast Servers by Key
    private static final Map<String, BlastServer> blastContainer = new HashMap<>(2);

    /**
     * Minimum Blast Server.
     * <p>
     * Default configs will be used.
     *
     * @return
     */
    public static BlastServer blast() {
        return new Builder().build();
    }

    public static BlastServer blast(BlastModule... modules) {
        Builder builder = new Builder();
        for (BlastModule module : modules) {
            builder.installModule(module);
        }
        return builder.build();
    }

    public static BlastServer getBlastInstance(String key) {
        return blastContainer.get(key);
    }

    public static void storeBlastInstance(String key, BlastServer server) {
        blastContainer.put(key, server);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private BlastProperties properties;
        private final List<BlastModule> modules;
        private ObjectMapper objectMapper;
        private LmEventBus eventbus;

        Builder() {
            modules = new ArrayList<>(5);
        }

        public Builder setEventBus(LmEventBus eventbus) {
            this.eventbus = eventbus;
            return this;
        }

        public Builder setProperties(BlastProperties properties) {
            this.properties = properties;
            return this;
        }

        public Builder setObjectMapper(ObjectMapper mapper) {
            this.objectMapper = mapper;
            return this;
        }

        public Builder installModule(BlastModule module) {
            modules.add(module);
            return this;
        }

        public BlastServer build() {

            if (properties == null) {
                BlastPropertiesImpl jsonProperties = new BlastPropertiesImpl();
                jsonProperties.load();
                properties = jsonProperties;
            }

            if (objectMapper == null) {
                objectMapper = ObjectMapperFactory.createDefaultObjectMapper();
                objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
            }

            BlastServerImpl blast;
            if (eventbus == null) {
                blast = new BlastServerImpl();
            } else {
                blast = new BlastServerImpl(eventbus);
            }

            blast.setProperties(properties);
            blast.setObjectMapper(objectMapper);

            for (BlastModule module : modules) {
                blast.installModule(module);
            }

            return blast;
        }
    }

    public static GUSLLogger getLogger() {
        return logger;
    }
}
