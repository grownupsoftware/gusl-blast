/*
 * Grownup Software Limited.
 */
package gusl.blast.blaster;

import gusl.blast.client.BlastServerClient;
import gusl.core.annotations.DocEvent;

import static gusl.core.annotations.DocEvent.EventSource.APPLICATION;

/**
 * @author dhudson - Mar 24, 2017 - 10:12:25 AM
 */
@DocEvent(source = APPLICATION, description = "Enqueue a message to be written to a client")
public class BlasterQueueEvent {

    private final BlastServerClient client;
    private final byte[] bytes;
    private Object pojo;

    public BlasterQueueEvent(BlastServerClient client, byte[] bytes) {
        this.client = client;
        this.bytes = bytes;
    }

    public BlastServerClient getClient() {
        return client;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public Object getPojo() {
        return pojo;
    }

    public void setPojo(Object pojo) {
        this.pojo = pojo;
    }
}
