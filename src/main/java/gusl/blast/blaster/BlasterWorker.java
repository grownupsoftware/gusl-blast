/*
 * Grownup Software Limited.
 */
package gusl.blast.blaster;

import gusl.blast.client.BlastServerClient;
import gusl.blast.client.ClosingReason;
import lombok.CustomLog;

import java.io.IOException;

/**
 * Blast Worker.
 * <p>
 * A worker lives for the duration of the blaster. It sits and awaits on the
 * blaster queue, once its take is successful, bytes are written.
 *
 * @author dhudson - Mar 24, 2017 - 11:49:43 AM
 */
@CustomLog
public class BlasterWorker implements Runnable {

    private final BlasterQueue queue;
    private boolean isRunning;
    private long startedWriting;

    //TODO:  This should be a client timeout and not system wide.
    //For example moble clients, may have a longer timeout than desktops.
    private final long writeTimeout;

    private Thread serviceThread;

    BlasterWorker(BlasterQueue queue, long writeTimeout) {
        this.queue = queue;
        this.writeTimeout = writeTimeout;
    }

    @Override
    public void run() {

        serviceThread = Thread.currentThread();

        isRunning = true;

        BlasterQueueEvent event;

        while (isRunning) {
            try {
                event = queue.take();
                try {
                    BlastServerClient client = event.getClient();
                    if (client == null) {
                        continue;
                    }

                    try {
                        // Most engines will have this async, but we do need to make sure that clients do not block a blaster
                        startedWriting = System.currentTimeMillis();

                        // Write to the client
                        client.write(event.getBytes());

                        // Lets check to see if we were interrupted
                        if (Thread.interrupted()) {
                            logger.warn("Client {} taking too long to write adding back pressure", client.getAddressAndID());
                        }

                    } catch (IOException ex) {
                        // I think that this should close the client and send.
                        logger.debug("Client {} threw IO Exception", client.getAddressAndID(), ex);
                        // This will trigger a ClientClosedEvent, which will remove stuff from the queue
                        client.close(ClosingReason.IO_EXCEPTION);
                    } catch (Throwable t) {
                        // What ever happens, don't let the worker die
                        logger.warn("Exception thrown in Blaster Worker.", t);
                    }
                } finally {
                    startedWriting = 0;
                    queue.resetRunningClient(event.getClient().getClientID());
                }
            } catch (InterruptedException ex) {
                isRunning = false;
            }
        }
    }

    public boolean isTakingTooLong() {
        if (startedWriting > 0) {
            if (startedWriting + writeTimeout > System.currentTimeMillis()) {
                return true;
            }
        }
        return false;
    }

    public Thread getServiceThread() {
        return serviceThread;
    }

    public long getStartedWritingTime() {
        return startedWriting;
    }

    public void shutdown() {
        isRunning = false;
    }
}
