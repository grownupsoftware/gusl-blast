/*
 * Grownup Software Limited.
 */
package gusl.blast.blaster;

import gusl.blast.client.BlastServerClient;
import gusl.core.annotations.DocEvent;

import java.util.List;

import static gusl.core.annotations.DocEvent.EventSource.SERVER;

/**
 *
 * @author dhudson - Apr 27, 2017 - 11:05:39 AM
 */
@DocEvent(source = SERVER, description = "This event contains what was on the blaster queue, when the client closed.")
public class RemovedBlasterEntriesEvent {

    private final BlastServerClient serverClient;
    private final List<BlasterQueueEvent> removedEvents;

    public RemovedBlasterEntriesEvent(BlastServerClient client, List<BlasterQueueEvent> events) {
        serverClient = client;
        removedEvents = events;
    }

    public BlastServerClient getClient() {
        return serverClient;
    }

    public List<BlasterQueueEvent> getRemovedEvents() {
        return removedEvents;
    }
}
