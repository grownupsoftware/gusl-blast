/*
 * Grownup Software Limited.
 */
package gusl.blast.blaster;

import gusl.blast.client.BlastServerClient;
import gusl.blast.client.ClientClosedEvent;
import gusl.blast.exception.BlastException;
import gusl.blast.message.BroadcastEvent;
import gusl.blast.server.BlastServer;
import gusl.blast.server.config.BlastProperties;
import gusl.core.eventbus.OnEvent;
import gusl.core.executors.NamedFixedPoolExecutor;
import lombok.CustomLog;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;

/**
 * @author dhudson - Mar 24, 2017 - 10:18:52 AM
 */
@CustomLog
public class BlasterImpl implements Blaster {

    public static final String BLASTER_MODULE_ID = "co.gusl.blaster";

    private final BlasterQueue queue;
    private BlastProperties blastProperties;
    private ExecutorService blasterService;
    private Set<BlasterWorker> blasters;
    private int blastersSize;
    private BlasterMonitor monitor;

    public BlasterImpl() {
        queue = new BlasterQueue();
    }

    @Override
    public void configure(BlastServer server) {
        // Register thyself
        server.registerEventListener(this);
        blastProperties = server.getProperties();
        blastersSize = blastProperties.getBlastersThreadSize();
        blasterService = new NamedFixedPoolExecutor(blastProperties.getBlastersThreadSize(), "Blaster");
        blasters = new HashSet<>(blastersSize);
    }

    @Override
    public void startup() throws BlastException {
        logger.debug("Starting {} blasters", blastersSize);
        for (int i = 0; i < blastersSize; i++) {
            // TODO: 5 Second timeout ..  Needs to be moved to client or properties.
            BlasterWorker worker = new BlasterWorker(queue, 5000);
            blasters.add(worker);
            // This runs untils blaster is shut down
            blasterService.submit(worker);
        }

        //DH: Fix Me
        //monitor = new BlasterMonitor(blasters);
        //monitor.startup();
    }

    @Override
    public void shutdown() {
        logger.debug("Stopping blasters ...");

        if (monitor != null) {
            monitor.shutdown();
        }
        // Do this before the service, otherwise the service will never shut down
        for (BlasterWorker worker : blasters) {
            worker.shutdown();
        }

        if (blasterService != null) {
            blasterService.shutdown();
        }

    }

    @OnEvent(order = 40)
    public void handleClientClosedEvent(ClientClosedEvent event) {
        List<BlasterQueueEvent> removed = queue.removeEntriesFor(event.getClient().getClientID());
        if (!removed.isEmpty()) {

        }
    }

    @OnEvent(order = 40)
    public void handleBlasterQueueEvent(BlasterQueueEvent event) {
        queue.add(event);
    }

    @OnEvent(order = 40)
    public void broadcastEvent(BroadcastEvent event) {
        for (BlastServerClient client : event.getClients()) {
            queue.add(new BlasterQueueEvent(client, event.getMessage().getBytes()));
        }
    }

    @Override
    public BlasterQueue getBlasterQueue() {
        return queue;
    }

    @Override
    public ExecutorService getBlasterServer() {
        return blasterService;
    }

    @Override
    public Set<BlasterWorker> getBlasters() {
        return blasters;
    }

    @Override
    public String getModuleID() {
        return BLASTER_MODULE_ID;
    }

}
