/*
 * Grownup Software Limited.
 */
package gusl.blast.blaster;

import gusl.blast.Controllable;
import gusl.blast.module.BlastModule;

import java.util.Set;
import java.util.concurrent.ExecutorService;

public interface Blaster extends Controllable, BlastModule {

    BlasterQueue getBlasterQueue();

    ExecutorService getBlasterServer();

    Set<BlasterWorker> getBlasters();

}
