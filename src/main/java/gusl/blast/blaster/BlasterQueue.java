/*
 * Grownup Software Limited.
 */
package gusl.blast.blaster;

import lombok.CustomLog;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Handle Client Messages in order.
 * <p>
 * Only one client will be written to at a time, so no need to lock.
 *
 * @author dhudson - Mar 24, 2017 - 10:39:47 AM
 */
@CustomLog
public class BlasterQueue extends LinkedBlockingQueue<BlasterQueueEvent> {

    private static final long serialVersionUID = 8174900193073830878L;

    private final ReentrantLock theLock;
    private final Object sync = new Object();
    private volatile boolean isWaiting;

    private final List<String> rejectedClients = new ArrayList<>();
    private final Set<String> runningClients;

    BlasterQueue() {
        this(Integer.MAX_VALUE, false);
    }

    /**
     * Create a new keyed queue with the given fixed capacity and whether
     * locking is fair or not.
     *
     * @param capacity    Fixed queue capacity.
     * @param useFairLock If true all threads get a fair chance to take from the
     *                    queue, if false take will be faster but not fair meaning some threads
     *                    will wait longer than others.
     */
    BlasterQueue(int capacity, boolean useFairLock) {
        super(capacity);
        theLock = new ReentrantLock(useFairLock);
        runningClients = Collections.newSetFromMap(new ConcurrentHashMap<>());
    }

    @Override
    public BlasterQueueEvent take() throws InterruptedException {
        try {
            // At this point in time, only one thread will access this
            theLock.lock();
            while (true) {

                if (Thread.interrupted()) {
                    logger.warn("ERR001 - Throw InterruptedException - thread interupted");
                    throw new InterruptedException();
                }

                // Are we empty
                if (isEmpty()) {
                    isWaiting = true;
                    synchronized (sync) {
                        sync.wait(5l);
                    }
                    isWaiting = false;
                    continue;
                }

                // List in order
                Iterator<BlasterQueueEvent> iterator = iterator();

                BlasterQueueEvent entry;
                rejectedClients.clear();

                while (iterator.hasNext()) {
                    entry = iterator.next();
                    String clientID = entry.getClient().getClientID();

                    // Check to see if there has been an interruption
                    if (Thread.interrupted()) {
                        logger.warn("ERR002 - [{}] Throw InterruptedException - thread interupted", clientID);
                        throw new InterruptedException();
                    }

                    if (rejectedClients.contains(clientID)) {
                        // We have already gone past this one, so we will have to wait until we start the list again
                        continue;
                    }

                    if (runningClients.contains(clientID)) {
                        // Thread alreay running with this key
                        rejectedClients.add(clientID);
                        continue;
                    }

                    runningClients.add(clientID);
                    iterator.remove();
                    return entry;
                }
            }

        } finally {
            theLock.unlock();
        }
    }

    @Override
    public boolean offer(BlasterQueueEvent e) {
        boolean result = super.offer(e);
        if (isWaiting) {
            synchronized (sync) {
                sync.notify();
            }
        }
        return result;
    }

    /**
     * Needs to be called after execution, to allow for future executions of
     * this key.
     *
     * @param client to reset
     */
    void resetRunningClient(String clientID) {
        runningClients.remove(clientID);
    }

    /**
     * Remove all entries for this client.
     * <p>
     * Remove messages for a client.
     *
     * @param clientID
     */
    List<BlasterQueueEvent> removeEntriesFor(String clientID) {
        Iterator<BlasterQueueEvent> iterator = iterator();
        List<BlasterQueueEvent> events = new ArrayList<>();

        BlasterQueueEvent entry;

        while (iterator.hasNext()) {
            entry = iterator.next();
            if (clientID.equals(entry.getClient().getClientID())) {
                events.add(entry);
                iterator.remove();
            }
        }

        //resetRunningClient()
        return events;
    }

    boolean hasMoreMessage(String clientID) {
        return stream().anyMatch(k -> k.getClient().getClientID().equals(clientID));
    }
}
