package gusl.blast.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gusl.blast.server.config.Endpoint;
import gusl.blast.server.config.SocketConditioning;
import gusl.core.annotations.DocProperty;
import gusl.core.config.CoreCountDeserializer;
import gusl.core.utils.Platform;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class BlastServerConfig {
    public static final String DEFAULT_CONFIG_NAME = "ServerConfig.json";

    // Create new ones of these as they contain sensible defaults
    @DocProperty(description = "Blast Endpoint Pojo")
    public Endpoint endpoint = new Endpoint();

    @DocProperty(description = "Socket Conditioning Pojo")
    public SocketConditioning socketConditioning = new SocketConditioning();

    // Number of cores by default
    @DocProperty(description = "Number of threads for the event bus", defaultValue = "2")
    @JsonDeserialize(using = CoreCountDeserializer.class)
    public int eventBusThreadPoolSize = 2;

    @DocProperty(description = "Number of threads for the blasters", defaultValue = "Number of available cores")
    @JsonDeserialize(using = CoreCountDeserializer.class)
    public int blastersThreadPoolSize = Platform.getNumberOfCores();

}
