/*
 * Grownup Software Limited.
 */
package gusl.blast.message;

/**
 * @author dhudson
 */
public interface BlastMessage {

    public String getCmd();

}
