/*
 * Grownup Software Limited.
 */
package gusl.blast.client;

import gusl.core.annotations.DocEvent;

import static gusl.core.annotations.DocEvent.EventSource.CLIENT;

/**
 * Client Connecting Event.
 *
 * Any origin checking should be done on this event, and before the Client
 * Manager gets it.
 *
 * If for any reason, the connection is not valid, it is suggested that you
 * close the client and fail fast so that the client manager will not process
 * the client.
 *
 * @author dhudson - Mar 23, 2017 - 8:38:06 AM
 */
@DocEvent(source = CLIENT, description = "This event is produced by the Engine, when a new client is created.  The Client Manager, will listener to this event and add the client."
        + "Any client validation should be done before the client manager, which has an order of 40.")
public class ClientConnectingEvent {

    private final BlastServerClient serverClient;

    public ClientConnectingEvent(BlastServerClient client) {
        serverClient = client;
    }

    public BlastServerClient getClient() {
        return serverClient;
    }
}
