/*
 * Grownup Software Limited.
 */
package gusl.blast.client;

import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * Web Socket Request Details.
 *
 * @author dhudson - Mar 28, 2017 - 4:07:58 PM
 */
public interface WebSocketRequestDetails {

    public Map<String, List<String>> getHeaders();

    public String getQueryString();

    public String getOrigin();

    public String getRequestPath();

    public Map<String, List<String>> getParams();

    public URI getRequestURI();

    public String getRemoteAddress();
}
