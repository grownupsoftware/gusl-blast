/*
 * Grownup Software Limited.
 */
package gusl.blast.client;

import gusl.blast.Controllable;
import gusl.blast.module.BlastModule;
import java.util.List;

/**
 * Client Manager.
 *
 * Handles, err, the clients.
 *
 * @author dhudson - Mar 21, 2017 - 11:13:17 AM
 */
public interface ClientManager extends Controllable, BlastModule {

    public List<BlastServerClient> getClients();

    public BlastServerClient getClientByID(String id);
}
