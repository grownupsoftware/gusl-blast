/*
 * Grownup Software Limited.
 */
package gusl.blast.client;

import gusl.blast.exception.BlastException;
import java.io.IOException;
import java.util.Map;

/**
 * All Blast Clients must implement this.
 *
 * @author dhudson - Mar 21, 2017 - 10:24:17 AM
 */
public interface BlastServerClient {

    /**
     * Close the client underlying socket connection.
     *
     * @param reason
     */
    public void close(ClosingReason reason);

    /**
     * Return a unique ID of this client.
     *
     * If operating in a cluster, this should be unique for the cluster.
     *
     * @return unique ID
     */
    public String getClientID();

    /**
     * Return the remote address.
     *
     * @return remote address if known, or null
     */
    public String getRemoteAddress();

    /**
     * Write bytes to the web socket in a frame.
     *
     * This is called via the blasters, in a thread safe manor, this should not
     * be used by anything else.
     *
     * @param bytes
     * @throws IOException
     */
    public void write(byte[] bytes) throws IOException;

    /**
     * Add a message to the client queue.
     *
     * This is how implementation should write to the underlying WebSocket.
     *
     * @param bytes to write
     */
    public void queueMessage(byte[] bytes);

    /**
     * Add a message to the client queue
     *
     * @param message
     * @throws BlastException
     */
    public void queueMessage(Object message) throws BlastException;

    public default String getAddressAndID() {
        StringBuilder builder = new StringBuilder(50);
        builder.append(getRemoteAddress()).append(":").append(getClientID());
        return builder.toString();
    }

    public Map<String, Object> getUserProperties();

    public Object getUserProperty(String key);

    public void addUserProperty(String key, Object value);

    /**
     * Retrieves the request details for the WebSocket Upgrade.
     *
     * @return websocket request details.
     */
    public WebSocketRequestDetails getRequestDetails();
}
