/*
 * Grownup Software Limited.
 */
package gusl.blast.client;

import gusl.core.annotations.DocEvent;

import static gusl.core.annotations.DocEvent.EventSource.CLIENT;

/**
 * Client Closed Event.
 * 
 * For reasons given.
 * 
 * @author dhudson - Mar 23, 2017 - 8:58:58 AM
 */
@DocEvent(source = CLIENT, description = "This event is produced by the client when it has been closed for any reason.")
public class ClientClosedEvent {

    private final BlastServerClient client;
    private final ClosingReason reason;

    public ClientClosedEvent(BlastServerClient client, ClosingReason reason) {
        this.client = client;
        this.reason = reason;
    }

    public BlastServerClient getClient() {
        return client;
    }

    public ClosingReason getReason() {
        return reason;
    }
    
}
