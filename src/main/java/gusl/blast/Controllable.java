/*
 * Grownup Software Limited.
 */
package gusl.blast;

import gusl.blast.exception.BlastException;

/**
 * @author dhudson - Mar 22, 2017 - 1:35:53 PM
 */
public interface Controllable {

    void startup() throws BlastException;

    void shutdown();
}
