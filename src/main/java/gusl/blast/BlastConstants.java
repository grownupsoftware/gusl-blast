/*
 * Grownup Software Limited.
 */
package gusl.blast;

/**
 * Constants used by Blast.
 *
 * @author dhudson - Mar 29, 2017 - 12:19:57 PM
 */
public interface BlastConstants {

    /**
     * {@value }
     */
    static final String BLAST_ROUTE_PATH = "/blast";

    /**
     * {@value }
     */
    static final String ORIGIN_HEADER_KEY = "Origin";

    /**
     * {@value }
     */
    static final String UPGRADE_HEADER_KEY = "Upgrade";

    /**
     * {@value }
     */
    static final String UPGRADE_WEBSOCKET_VALUE = "websocket";

    /**
     * {@value }
     */
    static final String X_FORWARDED_FOR_HEADER_KEY = "X-Forwarded-For";

    /**
     * {@value }
     */
    static final String REMOTE_ADDRESS_KEY = "Blast-Remote-Address";

    /**
     * {@value }
     */
    static final String REMOTE_PORT_KEY = "Blast-Remote-Port";

}
