/*
 * Grownup Software Limited.
 */
package gusl.blast.command;

import gusl.blast.message.BlastMessage;
import gusl.core.tostring.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class CommandMessage implements BlastMessage {

    public String cmd;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
