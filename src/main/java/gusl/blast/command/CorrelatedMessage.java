/*
 * Grownup Software Limited.
 */
package gusl.blast.command;

/**
 *
 * @author grant
 */
public class CorrelatedMessage extends CommandMessage {

    protected Long correlationId;

    public CorrelatedMessage() {
    }

    public Long getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(Long correlationId) {
        this.correlationId = correlationId;
    }

    @Override
    public String toString() {
        return "CorrelatedMessage{" + "correlationId=" + correlationId + '}';
    }

}
