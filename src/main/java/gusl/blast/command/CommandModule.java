/*
 * Grownup Software Limited.
 */
package gusl.blast.command;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.blast.client.ClientInboundMessageEvent;
import gusl.blast.module.BlastModule;
import gusl.blast.server.BlastServer;
import gusl.core.eventbus.LmEventBus;
import gusl.core.eventbus.OnEvent;
import lombok.CustomLog;

import java.io.IOException;
import java.util.HashMap;

/**
 * @author dhudson - May 8, 2017 - 10:33:16 AM
 */
@CustomLog
public class CommandModule implements BlastModule {

    public static final String COMMAND_DECODER_MODULE_ID = "co.gusl.command.decoder";

    private final HashMap<String, CommandHandlerDetails> decoderMap;
    private ObjectMapper objectMapper;
    private LmEventBus eventBus;

    public CommandModule() {
        decoderMap = new HashMap<>();
    }

    @Override
    public void configure(BlastServer server) {
        eventBus = server.getEventBus();
        objectMapper = server.getObjectMapper();
        eventBus.register(this);
    }

    public void registerCommand(String command, Class eventClass, Class dataClass, Object eventHandler) {
        registerCommand(command, eventClass, dataClass);
        eventBus.register(eventHandler);
    }

    public void registerCommand(String command, Class eventClass, Class dataClass) {
        register(new CommandHandlerDetails() {
            @Override
            public String getCommand() {
                return command;
            }

            @Override
            public Class getMessagePojo() {
                return dataClass;
            }

            @Override
            public Class getEventClass() {
                return eventClass;
            }
        });
    }

    public void register(CommandHandlerDetails details) {
        if (decoderMap.containsKey(details.getCommand())) {
            logger.debug("Registering a command of an already existing command {}", details.getCommand());
        }
        decoderMap.put(details.getCommand(), details);
    }

    @OnEvent(order = 40)
    public void handleInboudMessage(ClientInboundMessageEvent event) {
        JsonNode node;
        try {
            node = objectMapper.readTree(event.getMessage());
            JsonNode cmdNode = node.get("cmd");
            if (cmdNode == null) {
                logger.warn("No 'cmd' present in parent json object {}", event.getMessage());
                return;
            }

            String cmd = cmdNode.asText();
            logger.debug("Command of ... {}", cmd);

            CommandHandlerDetails details = decoderMap.get(cmd);

            if (details == null) {
                logger.warn("No commander handler for {}", cmd);
                return;
            }

            try {
                // Lets create the event
                BlastCommandEvent newEvent = (BlastCommandEvent) details.getEventClass().newInstance();
                newEvent.setBlastServerClient(event.getClient());
                logger.debug("Message Data: {} to Class: {}", node, details.getMessagePojo());
                newEvent.setCommandData(objectMapper.treeToValue(node, details.getMessagePojo()));

                // Lets put it on the bus
                eventBus.post(newEvent);
            } catch (InstantiationException | IllegalAccessException ex) {
                logger.warn("Unable to create Event Class {}", details.getEventClass(), ex);
            }

            // At this point I need to fire an event of what ever the command requires..
        } catch (IOException ex) {
            logger.warn("Unable to parse message [{}] from client [{}]", event.getMessage(), event.getClient().getAddressAndID(), ex);
        }
    }

    @Override
    public String getModuleID() {
        return COMMAND_DECODER_MODULE_ID;
    }

}
