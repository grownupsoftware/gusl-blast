/*
 * Grownup Software Limited.
 */
package gusl.blast.command;

/**
 *
 * @author grant
 * @param <T> The data class being returned
 */
public class CorrelatedResponse<T> {

    private Long correlationId;
    private T data;
    private String cmd;

    public CorrelatedResponse() {
    }

    public CorrelatedResponse(String cmd, Long correlationId, T data) {
        this.correlationId = correlationId;
        this.data = data;
        this.cmd = cmd;
    }

    public Long getCorrelationId() {
        return correlationId;
    }

    public void setCorrelatedId(Long correlatedId) {
        this.correlationId = correlatedId;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    @Override
    public String toString() {
        return "CorrelatedResponseDO{" + "correlationId=" + correlationId + ", data=" + data + ", cmd=" + cmd + '}';
    }

}
