package gusl.blast.external.client;

public interface ExternalClientEventHandler {

    void onOpen();

    void onMessage(WebSocketMessage message);

    void onClose();

    void onError(WebSocketException e);

    void onLogMessage(String msg);
}
