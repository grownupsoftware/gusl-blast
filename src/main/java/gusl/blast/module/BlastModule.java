/*
 * Grownup Software Limited.
 */
package gusl.blast.module;

import gusl.blast.exception.BlastException;
import gusl.blast.server.BlastServer;

/**
 * @author dhudson - Mar 23, 2017 - 4:37:44 PM
 */
public interface BlastModule {

    public void configure(BlastServer server) throws BlastException;

    public String getModuleID();

}
