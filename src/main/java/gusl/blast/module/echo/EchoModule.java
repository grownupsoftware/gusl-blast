/*
 * Grownup Software Limited.
 */
package gusl.blast.module.echo;

import gusl.blast.command.CommandModule;
import gusl.blast.exception.BlastException;
import gusl.blast.module.BlastModule;
import gusl.blast.server.BlastServer;

/**
 * @author grant
 */
public class EchoModule implements BlastModule {

    public static final String ECHO_MODULE_ID = "co.gusl.echo";

    @Override
    public void configure(BlastServer server) throws BlastException {

        // Echo requires the Command Module
        CommandModule commandModule = (CommandModule) server.requireModule(CommandModule.COMMAND_DECODER_MODULE_ID, CommandModule.class);

        // Echo command handler
        commandModule.registerCommand("echo", EchoCommandEvent.class, EchoMessage.class, new EchoCommandHandler());
    }

    @Override
    public String getModuleID() {
        return ECHO_MODULE_ID;
    }
}
