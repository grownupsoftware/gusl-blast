/*
 * Grownup Software Limited.
 */
package gusl.blast.module.echo;

import gusl.core.eventbus.OnEvent;
import lombok.CustomLog;

/**
 * @author grant
 */
@CustomLog
public class EchoCommandHandler {

    public EchoCommandHandler() {
    }

    @OnEvent
    public void handleEcho(EchoCommandEvent event) {
        logger.info("Echo Received From {}", event.getClient().getAddressAndID());
        // It is possible that the message field is missing from the command data
        String message = event.getCommandData().getMessage();
        if (message != null) {
            // Send the message back to the client, via queues and blasters
            event.getClient().queueMessage(message.getBytes());
        } else {
            logger.info("Missing message field from Echo Message");
        }
    }
}
