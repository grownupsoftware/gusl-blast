/*
 * Grownup Software Limited.
 */
package gusl.blast.module.ping;

import gusl.core.eventbus.OnEvent;
import lombok.CustomLog;

/**
 * This is what gets called by the event bus.
 *
 * @author dhudson - Mar 23, 2017 - 11:46:40 AM
 */
@CustomLog
public class PingCommandHandler {

    public PingCommandHandler() {
    }

    @OnEvent
    public void handlePing(PingCommandEvent event) {
        logger.debug("I have event .. {}", event.getCommandData());
    }

}
