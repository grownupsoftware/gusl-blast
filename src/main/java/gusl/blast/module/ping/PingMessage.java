/*
 * Grownup Software Limited.
 */
package gusl.blast.module.ping;

import gusl.blast.command.CommandMessage;

/**
 * Ping Message.
 * <p>
 * This is the inbound JSON message
 *
 * @author dhudson - Mar 23, 2017 - 11:44:57 AM
 */
public class PingMessage extends CommandMessage {

    public PingMessage() {

    }
}
