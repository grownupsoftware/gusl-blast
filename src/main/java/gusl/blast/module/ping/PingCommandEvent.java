/*
 * Grownup Software Limited.
 */
package gusl.blast.module.ping;

import gusl.blast.command.BlastCommandEvent;

/**
 * This is what gets put on the event bus.
 *
 * @author dhudson - Mar 23, 2017 - 11:50:40 AM
 */
public class PingCommandEvent extends BlastCommandEvent<PingMessage> {

    public PingCommandEvent() {
    }

}
