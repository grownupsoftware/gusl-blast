package gusl.blast.model;

import gusl.core.tostring.ToString;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ServerToClientMessageBO<T> {

    private String cmd;

    private T data;

    private Boolean hasError;

    private String errorMessage;

    private Long correlationId;


    public static <T> ServerToClientMessageBO of(String cmd, T data) {
        return ServerToClientMessageBO.builder().cmd(cmd).data(data).build();
    }
    public static ServerToClientMessageBO error(String errorMessage, boolean error) {
        return ServerToClientMessageBO.builder()
                .cmd("response.error")
                .hasError(error)
                .errorMessage(errorMessage)
                .build();
    }


    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
