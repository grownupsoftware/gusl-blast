package gusl.blast.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationWsDTO {
    private String message;
    private Type type;
    private Integer timeout;
    private Long correlationId;

    public enum Type {
        success, warning, info, error
    }
}


