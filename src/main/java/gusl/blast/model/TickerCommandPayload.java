package gusl.blast.model;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.HashMap;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TickerCommandPayload {

    //it allows to connect request and response via websocket
    private Long correlationId;

    private String collection;
    private Long adminId;
    private String tickerId;
    private HashMap<String, Object> data;
    private String cmd;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
