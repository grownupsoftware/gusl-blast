package gusl.blast.model;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ConflatedServerToClientMessageBO<T> {
    private String uniqueId;

    private String cmd;

    private T data;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
