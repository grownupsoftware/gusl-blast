package gusl.blast.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TickerMessageWsDTO<T> {
    private Action action;
    private Status status;
    private T data;
    private String collection;

    public enum Status {
        OK, ERROR
    }

    public enum Action {
        ADD,
        UPDATE,
        REMOVE,
        GET,
        ALL,
        CLEAR,
        POPULATE,
        DROP_ALL
    }
}
