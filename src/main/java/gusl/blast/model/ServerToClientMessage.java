package gusl.blast.model;

import gusl.core.tostring.ToString;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ServerToClientMessage<T> {

    private String cmd;

    private T data;

    private Boolean hasError;

    private String errorMessage;

    public ServerToClientMessage(String cmd, T data) {
        this.cmd = cmd;
        this.data = data;
    }

    public ServerToClientMessage(String errorMessage, boolean error) {
        this.cmd = "response.error";
        this.hasError = error;
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
