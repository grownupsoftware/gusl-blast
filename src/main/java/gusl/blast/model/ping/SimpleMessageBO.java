package gusl.blast.model.ping;

import gusl.core.tostring.ToString;

public class SimpleMessageBO<T> {

    private String cmd;

    private T data;

    private Boolean hasError;

    private String errorMessage;

    public SimpleMessageBO() {
    }

    public SimpleMessageBO(String cmd, T data) {
        this.cmd = cmd;
        this.data = data;
    }

    public SimpleMessageBO(String errorMessage, boolean error) {
        this.cmd = "response.error";
        this.hasError = error;
        this.errorMessage = errorMessage;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Boolean getHasError() {
        return hasError;
    }

    public void setHasError(Boolean hasError) {
        this.hasError = hasError;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
