package gusl.blast.model.ping;

import gusl.blast.command.BlastCommandEvent;
import gusl.core.tostring.ToString;

public class PingBE extends BlastCommandEvent<PingBO> {

    public static final String COMMAND_NAME = "ping";

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
