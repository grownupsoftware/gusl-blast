/*
 * Grownup Software Limited.
 */
package gusl.blast.exception;

import gusl.core.exceptions.GUSLException;

/**
 * General catch all for exceptions.
 * <p>
 * More detailed exceptions should be sub classed from this exception.
 * <p>
 * Created on Mar 15, 2017, 11:48:04 AM
 *
 * @author dhudson
 */
public class BlastException extends GUSLException {

    /**
     * Constant {@value}
     */
    private static final long serialVersionUID = 6359601744856420338L;

    /**
     * Creates a new instance of <code>GuslException</code> with the given error
     * message and the exception.
     *
     * @param message for the exception
     * @param ex      what caused the exception
     */
    public BlastException(String message, Throwable ex) {
        super(message, ex);
    }

    /**
     * Creates a new instance of <code>GuslException</code> with an error
     * message.
     *
     * @param message for the exception
     */
    public BlastException(String message) {
        super(message);
    }
}
