package gusl.blast.tickers.events;

import gusl.annotations.form.BaseActionConfig;
import gusl.annotations.form.TickerCommandConfig;
import gusl.blast.command.BlastCommandEvent;
import gusl.blast.model.TickerCommandPayload;

@TickerCommandConfig(
        base = @BaseActionConfig(
                successMessage = "Ticker unlocked successfully",
                buttonLabel = "Unlock ticker",
                jsEditCondition = "!(data.locked && data.locked.length > 0)"
        ),
        command = UnlockTickerEvent.UNLOCK_COMMAND,
        fontAwesomeIcon = "unlock",
        requireConfirmation = false,
        quickAccess = true
)
public class UnlockTickerEvent extends BlastCommandEvent<TickerCommandPayload> {
    public final static String UNLOCK_COMMAND = "unlock";
}
