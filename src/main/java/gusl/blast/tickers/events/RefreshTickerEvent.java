package gusl.blast.tickers.events;

import gusl.annotations.form.BaseActionConfig;
import gusl.annotations.form.TickerCommandConfig;
import gusl.blast.command.BlastCommandEvent;
import gusl.blast.model.TickerCommandPayload;

@TickerCommandConfig(
        base = @BaseActionConfig(
                successMessage = "Refreshed",
                buttonLabel = "refresh",
                jsEditCondition = "true"
        ),
        fontAwesomeIcon = "sync-alt",
        command = RefreshTickerEvent.REFRESH_COMMAND
)
public class RefreshTickerEvent extends BlastCommandEvent<TickerCommandPayload> {
    public final static String REFRESH_COMMAND = "refresh";
}
