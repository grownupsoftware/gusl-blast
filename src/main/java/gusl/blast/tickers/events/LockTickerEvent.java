package gusl.blast.tickers.events;

import gusl.annotations.form.BaseActionConfig;
import gusl.annotations.form.TickerCommandConfig;
import gusl.blast.command.BlastCommandEvent;
import gusl.blast.model.TickerCommandPayload;

@TickerCommandConfig(
        base = @BaseActionConfig(
                successMessage = "Ticker locked successfully",
                buttonLabel = "Lock ticker",
                jsEditCondition = "data.locked && data.locked.length > 0"
        ),
        fontAwesomeIcon = "lock",
        command = LockTickerEvent.LOCK_COMMAND,
        requireConfirmation = false,
        quickAccess = true
)
public class LockTickerEvent extends BlastCommandEvent<TickerCommandPayload> {
    public final static String LOCK_COMMAND = "lock";
}
