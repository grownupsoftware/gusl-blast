package gusl.blast.tickers.shared;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LockedByUserDTO {
    private String email;
    private String image;
    private boolean markedToRemove;
}
