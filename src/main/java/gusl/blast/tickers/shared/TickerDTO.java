package gusl.blast.tickers.shared;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gusl.core.tostring.ToString;
import gusl.core.utils.StringUtils;
import gusl.model.Identifiable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TickerDTO<T extends Identifiable<String>> implements Identifiable<String> {

    private LockedByUserDTO locked;
    private T data;

    public TickerDTO() {
        locked = new LockedByUserDTO();
    }

    public TickerDTO(T data) {
        this.data = data;
        locked = new LockedByUserDTO();
    }

    public TickerDTO(LockedByUserDTO locked, T data) {
        this.data = data;
        this.locked = locked;
    }

    @Override
    public String getId() {
        return data.getId();
    }

    @Override
    public void setId(String aLong) {
        throw new UnsupportedOperationException("Cannot set exception");
    }

    @JsonIgnore
    public boolean isTickerLocked() {
        return locked != null && StringUtils.isNotBlank(locked.getEmail());
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
