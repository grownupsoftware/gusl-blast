package gusl.blast.tickers.shared;

public class TickerActionDTO {
    private String label;
    private String command;

    public TickerActionDTO() {
    }

    public TickerActionDTO(String label, String command) {
        this.label = label;
        this.command = command;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }
}
