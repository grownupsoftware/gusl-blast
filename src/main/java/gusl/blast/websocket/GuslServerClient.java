/* Copyright lottomart */
package gusl.blast.websocket;

import gusl.blast.client.AbstractBlastServerClient;
import gusl.blast.client.ClosingReason;
import gusl.blast.client.WebSocketRequestDetails;
import gusl.blast.server.BlastServer;
import gusl.core.utils.IOUtils;

import javax.websocket.Session;
import java.io.IOException;

/**
 * @author grant
 */
public class GuslServerClient extends AbstractBlastServerClient {

    private final Session session;
    private final String clientID;

    public GuslServerClient(Session session, BlastServer server, WebSocketRequestDetails details) {
        super(server, details);
        this.session = session;
        clientID = createShortClientID();
    }

    @Override
    public void close(ClosingReason reason) {
        if (session.isOpen()) {
            IOUtils.closeQuietly(session);
        }
        postClientClosed(reason);
    }

    @Override
    public String getClientID() {
        //return session.getId();
        return clientID;
    }

    @Override
    public void write(byte[] bytes) throws IOException {
        if (session.isOpen()) {
            session.getBasicRemote().sendText(new String(bytes));
        }
    }

    @Override
    public void queueMessage(byte[] bytes) {
        postQueuedMessageEvent(bytes);
    }

}
