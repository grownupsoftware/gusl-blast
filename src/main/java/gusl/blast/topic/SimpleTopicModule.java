package gusl.blast.topic;

import gusl.blast.command.CommandModule;
import gusl.blast.exception.BlastException;
import gusl.blast.module.BlastModule;
import gusl.blast.server.BlastServer;

public class SimpleTopicModule implements BlastModule {

    public static final String TOPIC_MODULE_ID = "gusl.blast.topic";

    private final TopicManager topicManager;

    public SimpleTopicModule() {
        topicManager = new SimpleTopicManagerImpl();
    }

    @Override
    public void configure(BlastServer server) throws BlastException {
        CommandModule commandModule = (CommandModule) server.requireModule(CommandModule.COMMAND_DECODER_MODULE_ID, CommandModule.class);

        // Register commands ..
        commandModule.registerCommand("send", SendTopicMessageEvent.class, SendTopicMessage.class);
        commandModule.registerCommand("subscribe", SubscribeMessageEvent.class, TopicMessage.class);
        commandModule.registerCommand("unsubscribe", UnSubscribeMessageEvent.class, TopicMessage.class);

        // Lets get the topic manager to listen to the event bus
        server.getEventBus().register(topicManager);
    }

    @Override
    public String getModuleID() {
        return TOPIC_MODULE_ID;
    }

}
