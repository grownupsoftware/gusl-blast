package gusl.blast.topic;

import gusl.blast.client.BlastServerClient;

public interface TopicManager {

    public void addTopic(Topic topic);
    
    public void removeTopic(Topic topic);
 
    public void handleOnMessage(BlastServerClient client, SendTopicMessage message);
}
