package gusl.blast.topic;

import gusl.blast.client.BlastServerClient;

public interface Topic {
    
    public String getName();
    
    public void sendMessage(SendTopicMessage message);
    
    public void subscribe(BlastServerClient client);
    
    public void unSubscribe(BlastServerClient client);
}
