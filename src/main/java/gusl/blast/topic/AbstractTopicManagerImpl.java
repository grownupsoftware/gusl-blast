package gusl.blast.topic;

public abstract class AbstractTopicManagerImpl implements TopicManager {

    public AbstractTopicManagerImpl() {
    }

    public abstract Topic getTopic();

    public abstract Topic getTopic(String topicPath);

}
