package gusl.blast.topic;

import gusl.blast.command.CommandMessage;
import gusl.core.tostring.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TopicMessage extends CommandMessage {

    public String topic;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
