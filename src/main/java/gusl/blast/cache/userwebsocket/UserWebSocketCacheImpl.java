package gusl.blast.cache.userwebsocket;

import gusl.blast.cache.userwebsocket.model.UserWebSocketDO;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import org.jvnet.hk2.annotations.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

@Service
public class UserWebSocketCacheImpl implements UserWebSocketCache {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    private final ConcurrentHashMap<String, Set<UserWebSocketDO>> theCustomerWebSocketMap = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, UserWebSocketDO> clientWebSocketMap = new ConcurrentHashMap<>();

    @Override
    public void addConnection(UserWebSocketDO UserWebSocketDO) {
        logger.debug("adding connection :{}", UserWebSocketDO);
        addSessionToUser(UserWebSocketDO);
    }

    @Override
    public void close(String clientID) {
        logger.debug("close client: {}", clientID);
        final UserWebSocketDO UserWebSocketDO = clientWebSocketMap.get(clientID);
        if (nonNull(UserWebSocketDO)) {
            removeSessionFromUser(UserWebSocketDO.getId(), UserWebSocketDO.getSessionId());
        }
    }

    @Override
    public List<UserWebSocketDO> getAll() {
        return new ArrayList<>(clientWebSocketMap.values());
    }

    @Override
    public void remove(String playerId, String sessionId) {
        removeSessionFromUser(playerId, sessionId);
    }

    private void addSessionToUser(UserWebSocketDO UserWebSocketDO) {
        if (theCustomerWebSocketMap.containsKey(UserWebSocketDO.getId())) {
            logger.debug("Web sockets - adding player session: {}", UserWebSocketDO);
            theCustomerWebSocketMap.get(UserWebSocketDO.getId())
                    .add(UserWebSocketDO);
            clientWebSocketMap.put(UserWebSocketDO.getClientId(), UserWebSocketDO);
        } else {
            Set<UserWebSocketDO> userSockets = new HashSet<>();
            logger.debug("Web sockets - adding player session: {}", UserWebSocketDO);
            userSockets.add(UserWebSocketDO);
            theCustomerWebSocketMap.put(UserWebSocketDO.getId(), userSockets);
            clientWebSocketMap.put(UserWebSocketDO.getClientId(), UserWebSocketDO);
        }
    }

    private void removeSessionFromUser(String playerId, String sessionId) {
        doRemoveSessionFromUser(playerId, sessionId);
    }

    private void doRemoveSessionFromUser(String playerId, String sessionId) {
        if (theCustomerWebSocketMap.containsKey(playerId)) {
            final Set<UserWebSocketDO> playerConnections = theCustomerWebSocketMap.get(playerId);

            if (isNull(playerConnections) || playerConnections.isEmpty()) {
                theCustomerWebSocketMap.remove(playerId);
                return;
            }

            final Optional<UserWebSocketDO> optional = safeStream(playerConnections).filter(session -> session.getSessionId().equals(sessionId)).findAny();
            if (optional.isPresent()) {
                logger.debug("Web sockets - removing player session: {}", optional.get());
                playerConnections.remove(optional.get());
                clientWebSocketMap.remove(optional.get().getClientId());

                if (playerConnections.isEmpty()) {
                    theCustomerWebSocketMap.remove(playerId);
                    logger.debug("Web sockets - now removing player as no sessions", playerId);
                }
            }
        }
    }

    @Override
    public Optional<UserWebSocketDO> find(String playerId, String sessionId) {
        final Set<UserWebSocketDO> playerConnections = theCustomerWebSocketMap.get(playerId);
        // logger.debug("Web sockets - find player: {} session: {} - found: {} ", playerId, sessionId);
        return safeStream(playerConnections)
                .filter(session -> session.getSessionId().equals(sessionId)).findAny();
    }

    @Override
    public List<UserWebSocketDO> findAll(String playerId) {
        final Set<UserWebSocketDO> connections = theCustomerWebSocketMap.get(playerId);
        return safeStream(connections).collect(toList());
    }

}
