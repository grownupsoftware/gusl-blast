package gusl.blast.cache.userwebsocket;

import gusl.blast.cache.userwebsocket.model.UserWebSocketDO;
import org.jvnet.hk2.annotations.Contract;

import java.util.List;
import java.util.Optional;

@Contract
public interface UserWebSocketCache {

    void remove(String bettorId, String sessionId);

    void addConnection(UserWebSocketDO customerWebSocketDO);

    Optional<UserWebSocketDO> find(String bettorId, String sessionId);

    List<UserWebSocketDO> findAll(String bettorId);

    void close(String clientID);

    List<UserWebSocketDO> getAll();
}
