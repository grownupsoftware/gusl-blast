package gusl.blast.cache.userwebsocket.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gusl.blast.client.BlastServerClient;
import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.model.Identifiable;
import lombok.*;

import java.util.Date;
import java.util.Objects;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Wrapper class around player websocket connection to edge")
public class UserWebSocketDO implements Identifiable<String> {

    private String id;

    private String sessionId;

    private String username;

    private String sessionToken;

    @DocField(description = "The underlying web socket connection")
    @JsonIgnore
    private BlastServerClient client;

    private String clientId;

    private Integer buildId;

    private String releaseNumber;

    @DocField(description = "in a browser on a mobile device.")
    private boolean mobileWeb;

    @DocField(description = "desktop browser")
    private boolean desktop;

    @DocField(description = "ios device")
    private boolean ios;

    @DocField(description = "android device")
    private boolean android;

    private Date since;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserWebSocketDO that = (UserWebSocketDO) o;
        return sessionId.equals(that.sessionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sessionId);
    }

    @Override
    public String toString() {
        return "UserWebSocketDO {" +
                "userId=" + id +
                ", sessionId=" + sessionId +
                ", username='" + username + '\'' +
                ", sessionToken='" + (sessionToken == null ? "" : sessionToken.substring(sessionToken.length() - 5)) + '\'' +
                ", clientId='" + clientId + '\'' +
                '}';
    }
}
