package gusl.blast.cache;

import gusl.auth.model.loggedin.LoggedInUserDO;

import java.util.List;

public interface BlastCache {
    String getName();

    List getAll();

    List getAll(LoggedInUserDO loggedInUser);
}
