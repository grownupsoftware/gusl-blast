package gusl.blast.graph.client;

import gusl.blast.graph.core.utils.ModificationWatcher;
import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@NoArgsConstructor
@Getter
@Setter

public class ClientCollection {

    private boolean isList;
    private String key;
    private Map<String, Object> data;
    private List<Map<String, Object>> listData;
    private ModificationWatcher modificationWatcher;
    private String attachmentId;

    public ClientCollection(String attachmentId, String key, Map<String, Object> data, ModificationWatcher modificationWatcher) {
        this.attachmentId = attachmentId;
        this.key = key;
        this.data = data;
        this.isList = false;
        this.listData = null;
        this.modificationWatcher = modificationWatcher;
    }

    public ClientCollection(String attachmentId, String key, List<Map<String, Object>> listData, ModificationWatcher modificationWatcher) {
        this.attachmentId = attachmentId;
        this.key = key;
        this.listData = listData;
        this.isList = true;
        this.data = null;
        this.modificationWatcher = modificationWatcher;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
