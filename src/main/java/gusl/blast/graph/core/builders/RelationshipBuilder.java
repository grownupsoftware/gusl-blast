package gusl.blast.graph.core.builders;

import gusl.blast.exception.BlastException;
import gusl.blast.graph.core.GraphCollection;
import gusl.blast.graph.core.dataobjects.Relationship;
import gusl.blast.server.BlastServer;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RelationshipBuilder {

    private GraphCollection childCollection;

    private GraphCollection parentCollection;

    private BlastServer blastServer;

    /**
     * the field name on child that is the link to the parent
     *
     * @param fieldName
     * @throws BlastException
     */
    public void using(String fieldName) throws BlastException {
        if (!childCollection.getFieldNames().contains(fieldName)) {
            throw new BlastException("Connecting field name does not exist on child collection [" + fieldName + "]");
        }
        // need to validate field exists on child
        childCollection.addLink(blastServer, new Relationship(fieldName, parentCollection));
    }

}
