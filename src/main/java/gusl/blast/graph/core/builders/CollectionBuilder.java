package gusl.blast.graph.core.builders;

import gusl.blast.exception.BlastException;
import gusl.blast.graph.core.BlastGraph;
import gusl.blast.graph.core.GraphCollection;
import gusl.blast.model.IGraphData;
import gusl.blast.graph.core.utils.GraphUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;

import static gusl.core.utils.Utils.safeStream;

/**
 * @param <T> Type of data class
 * @param <K> Type of key field
 */
public class CollectionBuilder<T extends IGraphData, K> {

    private String name;
    private String keyfield;
    private Class<T> dataclass;
    private String[] includeFields;
    private String[] excludeFields;
    private final BlastGraph blastGraph;

    public CollectionBuilder(BlastGraph blastGraph) {
        this.blastGraph = blastGraph;
    }

    public CollectionBuilder name(String name) {
        this.name = name;
        return this;
    }

    public CollectionBuilder keyfield(String keyfield) {
        this.keyfield = keyfield;
        return this;
    }

    public CollectionBuilder dataclass(Class<T> dataclass) {
        this.dataclass = dataclass;
        return this;
    }

    public CollectionBuilder onlyFields(String... includeFields) {
        this.includeFields = includeFields;
        return this;
    }

    public CollectionBuilder excludeFields(String... excludeFields) {
        this.excludeFields = excludeFields;
        return this;
    }

    public GraphCollection<? extends IGraphData, K> build() throws BlastException {
        List<Field> classFields = GraphUtils.getFieldsFor(dataclass);
        Optional<Field> optional = safeStream(classFields).filter(field -> field.getName().equals(keyfield)).findFirst();
        if (!optional.isPresent()) {
            throw new BlastException("Failed to find type of field [" + keyfield + "]");
        }
        Class<String> keyType = (Class<String>) optional.get().getType();
        GraphCollection.validateKeyClass(keyType);
        GraphCollection graphCollection = blastGraph.createCollection(name, keyfield, dataclass, keyType, includeFields, excludeFields);
        return graphCollection;
    }

}
