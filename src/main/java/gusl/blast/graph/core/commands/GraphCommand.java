package gusl.blast.graph.core.commands;

import gusl.blast.command.CommandMessage;
import gusl.blast.graph.core.dataobjects.PathParameters;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GraphCommand extends CommandMessage {

    @DocField(description = "Correlation Id")
    private Long correlationId;

    @DocField(description = "key to graph")
    private String key;

    @DocField(description = "additional params")
    private PathParameters parameters;

    @DocField(description = "additional data")
    private Map<String, Object> data;

    @DocField(description = "attachment id")
    private String attachmentId;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
