package gusl.blast.graph.core;

import gusl.blast.client.BlastServerClient;
import gusl.blast.exception.BlastException;
import gusl.blast.graph.core.dataobjects.GuslGraph;
import gusl.blast.graph.core.dataobjects.PathParameters;
import gusl.blast.model.IGraphData;
import gusl.blast.server.BlastServer;
import gusl.core.annotations.DocField;
import org.jvnet.hk2.annotations.Contract;

import java.util.List;
import java.util.Map;

@Contract
public interface BlastGraph {

    @DocField(description = "Collection name from which all connected clients are attached to")
    String CLIENT_MASTER = "client-master";

    @DocField(description = "Edge name between client-master and client vertex")
    String CLIENT_MASTER_EDGE = "clients";

    @DocField(description = "All attachments are connected to the attach-master")
    String ATTACH_MASTER = "attach-master";

    @DocField(description = "The audit vertex - not implemented")
    String AUDIT_MASTER = "audit";

    @DocField(description = "Protected internal collections")
    public String[] INTERNAL_COLLECTIONS = new String[]{
            CLIENT_MASTER,
            ATTACH_MASTER,
            AUDIT_MASTER
    };

    void initialise(BlastServer blastServer);

    GuslGraph getGraph();

    /**
     * Gets root data
     *
     * @param params apply path parameters
     * @return map by collection of all collections
     * @throws BlastException
     */
    Map<String, Object> getRootData(PathParameters params) throws BlastException;

    /**
     * Get all data for a collection
     *
     * @param collectionName the name of the col
     * @param params         apply path parameters
     * @return all entities for this collection
     * @throws BlastException
     */
    List<Map<String, Object>> getCollectionData(String collectionName, PathParameters params) throws BlastException;

    /**
     * Gets data for a specific key - not collection related
     *
     * @param key    the value of the key
     * @param params
     * @return the data for this entity
     * @throws BlastException
     */
    Map<String, Object> getData(String key, PathParameters params) throws BlastException;

    /**
     * Removes a collection
     *
     * @param collectionName The name of the collection to remove - also removes
     *                       all underling data
     */
    void removeCollection(String collectionName);

    /**
     * Schema dump of collections and their settings - no collection data is
     * returned
     *
     * @return list of graph collection summary
     * @throws BlastException
     */
    List<GraphCollection> getCollections() throws BlastException;

    void addCollection(GraphCollection collection) throws BlastException;

    /**
     * Returns a builder for collection creation
     *
     * @return
     */
    <T extends IGraphData, K> GraphCollection.GraphCollectionBuilder<T, K> createCollection();

    /**
     * Creates the collection
     *
     * @param collectionName The name of the collection to create
     * @param keyname        The key name i.e. the name of the unique identifier -
     *                       normally 'id'
     * @param clazz          The Data Class i.e. a POJO that is associated with this
     *                       collection
     * @param keyType        The class of the key e.g. String,Long,Integer
     * @param includeFields  only include fields when building the collection
     * @param excludeFields  include all fields from POJO except these
     * @return A new Graph Collection
     * @throws BlastException
     */
    GraphCollection createCollection(String collectionName, String keyname, Class<? extends IGraphData> clazz, Class<String> keyType, String[] includeFields, String[] excludeFields) throws BlastException;

    /**
     * Get the data for an entity but only include children specified by the
     * 'includeRelationships'
     *
     * @param key                  The value of the Key
     * @param includeRelationships An array of relationships (edges) to be
     *                             returned
     * @return Map of the data for this entity
     * @throws BlastException
     */
    Map<String, Object> get(String key, String... includeRelationships) throws BlastException;

//    /**
//     * Remove an entity
//     *
//     * @param <K> The Type of key
//     * @param key The value of the Key
//     */
//    <K> void remove(K key);

    /**
     * Starting from a entity id bubble up through all parent relationships
     * (edges) until the parent collection is reached
     *
     * @param key The value of the Key
     * @throws BlastException
     */
    void bubbleUp(String key) throws BlastException;

    /**
     * @param client The blast server client
     * @param key    follows format of "collection" or
     *               "collection/keyfield:keyvalue" or
     *               "collection/keyfield:keyvalue/childcollection"
     * @param params additional parameters
     * @return Either Map<String,Object> or List<Map<String,Object>>
     * @throws BlastException
     */
    Object fetch(BlastServerClient client, String key, PathParameters params) throws BlastException;

    void closeClient(BlastServerClient client);

    void createClient(BlastServerClient client);

    /**
     * @param client       the blast server client
     * @param key          follows format of "collection" or
     *                     "collection/keyfield:keyvalue" or
     *                     "collection/keyfield:keyvalue/childcollection"
     * @param attachmentId client side unique identifier for this collection
     * @param params       additional parameters
     * @return Either Map<String,Object> or List<Map<String,Object>>
     * @throws BlastException
     */
    Object attachTo(BlastServerClient client, String attachmentId, String key, PathParameters params) throws BlastException;

    /**
     * No longer attach to this key i.e. no longer receive any updates
     *
     * @param client
     * @param attachmentId unique attachment Id
     * @throws BlastException
     */
    void detachFrom(BlastServerClient client, String attachmentId) throws BlastException;

    void detachFromAll(BlastServerClient client) throws BlastException;

    List<Map<String, Object>> getClientAttachments(BlastServerClient client) throws BlastException;

    void add(String key, Map<String, Object> data) throws BlastException;

    void update(String key, Map<String, Object> data) throws BlastException;

    void remove(String key) throws BlastException;

    void setBlastServer(BlastServer blastServer);

}
