package gusl.blast.graph.core.dataobjects;

import com.google.common.graph.EndpointPair;
import com.google.common.graph.MutableNetwork;
import com.google.common.graph.NetworkBuilder;
import gusl.blast.model.IGraphData;
import gusl.core.tostring.ToString;
import gusl.core.utils.IdGenerator;
import lombok.CustomLog;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@CustomLog
public class GuslGraph {

    MutableNetwork<GuslVertex, GuslEdge<? extends IGraphData>> theNetworkGraph = NetworkBuilder.directed().build();

    Map<String, GuslVertex> theVertexMap = new ConcurrentHashMap<String, GuslVertex>();

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    public GuslVertex getVertex(String key) {
        final GuslVertex vertex = theVertexMap.get(key);
//        if (isNull(vertex)) {
//            logger.info("Vertex not found. key: {} Map: {}",key, JsonUtils.prettyPrint(theVertexMap) );
//        }
        return vertex;
    }

    public GuslVertex addVertex(String key) {
        final GuslVertex vertex = getVertex(key);
        if (nonNull(vertex)) {
            return vertex;
        }
        final GuslVertex newVertex = GuslVertex.of(key);
        theNetworkGraph.addNode(newVertex);
        theVertexMap.put(key, newVertex);
        return newVertex;
    }

    public GuslEdge<? extends IGraphData> addEdge(Map<String, Object> properties, GuslVertex parent, GuslVertex child, String label) {
        final GuslEdge<? extends IGraphData> edge = GuslEdge.builder()
                .id(IdGenerator.generateUniqueNodeIdAsString())
                .properties(nonNull(properties) ? properties : new ConcurrentHashMap<>())
                .parentId(parent.getId())
                .childId(child.getId())
                .label(label)
                // .data(value)
                .build();
        theNetworkGraph.addEdge(parent, child, edge);
        return edge;
    }

    public void removeVertex(GuslVertex attachVertex) {

    }

    public List<GuslVertex> getVertices() {
        return null;
    }

    public List<GuslEdge<? extends IGraphData>> getEdges(GuslVertex vertex, GraphDirection direction) {
        if (direction == GraphDirection.OUT) {
            return new ArrayList<>(theNetworkGraph.outEdges(vertex));
        }
        return new ArrayList<>(theNetworkGraph.inEdges(vertex));
    }

    public List<GuslEdge<? extends IGraphData>> getEdges(GuslVertex vertex, GraphDirection direction, String edgeName) {
        List<GuslEdge<? extends IGraphData>> edges;
        if (direction == GraphDirection.OUT) {
            edges = new ArrayList<>(theNetworkGraph.outEdges(vertex));
        }
        edges = new ArrayList<>(theNetworkGraph.inEdges(vertex));
        if (isNull(edgeName)) {
            return edges;
        }
        return safeStream(edges).filter(edge -> edgeName.equals(edge.getLabel())).collect(Collectors.toList());

    }

    public GuslVertex getVertex(GuslEdge<? extends IGraphData> edge, GraphDirection direction) {
        final EndpointPair<GuslVertex> endpointPair = theNetworkGraph.incidentNodes(edge);
        if (direction == GraphDirection.IN) {
            return endpointPair.nodeV();
        }
        return endpointPair.nodeU();
    }
}
