package gusl.blast.graph.core.dataobjects;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;
import java.util.Set;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PathParameters {

    @DocField(description = "include child collections")
    private boolean includeChildren;

    @DocField(description = "only return this list of fields")
    private List<String> fields;

    @DocField(description = "Queries to be applied to the result")
    private List<QueryParameter> predicates;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    public boolean hasIncludeChildren() {
        return false;
    }
}
