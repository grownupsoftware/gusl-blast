package gusl.blast.graph.core.dataobjects;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Objects.nonNull;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class GuslVertex {

    // I'm think vertex could be an interface and then CacheVertexImpl, HazelcastVertexImpl etc

    public static GuslVertex of(String id) {
        return GuslVertex.builder().id(id).properties(new ConcurrentHashMap<>()).build();
    }

    @EqualsAndHashCode.Include
    private String id;

    @EqualsAndHashCode.Exclude
    private Set<String> propertyKeys;

    @EqualsAndHashCode.Exclude
    private Map<String, Object> properties;

    @EqualsAndHashCode.Exclude
    private String attachmentId;

    @EqualsAndHashCode.Exclude
    private String key;

    public String getProperty(String key) {
        return (String) properties.get(key);
    }

    public Object getPropertyValue(String key) {
        return properties.get(key);
    }

    public Set<String> getFieldNames(String key) {
        return (Set<String>) properties.get(key);
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    public void setProperty(String key, Set<String> fieldNames) {
        properties.put(key, fieldNames);
    }

    public void setProperty(String key, Object value) {
        if (nonNull(key) && nonNull(value)) {
            properties.put(key, value);
        }
    }

    public boolean haveProperty(String s) {
        return false;
    }
}
