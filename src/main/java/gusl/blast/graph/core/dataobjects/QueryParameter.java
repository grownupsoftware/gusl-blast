package gusl.blast.graph.core.dataobjects;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryParameter {

    @DocField(description = "The Field to do the comparison on")
    private String field;

    @DocField(description = "The operand e.g. = != > < ")
    private String operand;

    @DocField(description = "The value to check")
    private String value;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
