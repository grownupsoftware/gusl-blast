package gusl.blast.graph.core.dataobjects;

public enum Operation {
    ADD,
    UPDATE,
    REMOVE
}
