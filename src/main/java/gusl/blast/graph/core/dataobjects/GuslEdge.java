package gusl.blast.graph.core.dataobjects;

import gusl.blast.model.IGraphData;
import lombok.*;

import java.util.Map;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GuslEdge<T extends IGraphData> {
    private String id;
    private String childId;
    private String parentId;

    private String label;

    private T data;

    private Map<String, Object> properties;

    private String attachmentId;

    private String key;

    public String getProperty(String key) {
        return (String) properties.get(key);
    }

    public void setProperty(String key, Object value) {
        properties.put(key, value);
    }

}
