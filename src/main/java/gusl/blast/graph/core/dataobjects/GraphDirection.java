package gusl.blast.graph.core.dataobjects;

public enum GraphDirection {
    OUT,
    IN
}
