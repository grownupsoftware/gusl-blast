/*
 * Grownup Software Limited.
 */
package gusl.blast.graph.core.dataobjects;

import gusl.blast.graph.core.GraphCollection;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Relationship {

    private String fieldName;

    private GraphCollection parentCollection;


    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
