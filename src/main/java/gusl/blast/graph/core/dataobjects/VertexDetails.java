package gusl.blast.graph.core.dataobjects;

// import com.tinkerpop.blueprints.Vertex;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class VertexDetails {

    private boolean isRoot;
    private boolean isCollection;
    private GuslVertex vertex;
    private String fullPath;
    private String[] queryParams;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
