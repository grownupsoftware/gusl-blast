package gusl.blast.graph.core.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.blast.exception.BlastException;
import gusl.blast.graph.core.GraphCollection;
import gusl.blast.graph.core.dataobjects.*;
import gusl.blast.model.IGraphData;
import gusl.core.json.ObjectMapperFactory;
import lombok.CustomLog;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static gusl.blast.graph.core.BlastGraph.INTERNAL_COLLECTIONS;
import static gusl.blast.graph.core.GraphCollection.COLLECTION_EDGE_NAME;
import static gusl.core.json.ObjectMapperFactory.LOWER_CAMEL_CASE;
import static gusl.core.utils.Utils.safeStream;

@CustomLog
public class GraphUtils {

    private static final ObjectMapper theObjectMapper = ObjectMapperFactory.getObjectMapperWithNaming(LOWER_CAMEL_CASE);

    private final static TypeReference<HashMap<String, Object>> TYPE_REF = new TypeReference<>() {
    };
    private static final TypeReference<HashMap<String, Object>> MAP_TYPE_REF = new TypeReference<>() {
    };

    static {
        theObjectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
    }

    public static ObjectMapper getObjectMapper() {
        //FIXME - should get this from the Blast Server
        return theObjectMapper;
    }

    public static <T> List<T> jsonToList(String value, Class<T> clazz) throws BlastException {
        if (value == null || value.isEmpty()) {
            return new ArrayList<>();
        }

        TypeReference<List<T>> listRef = new TypeReference<>() {
        };
        try {
            return getObjectMapper().readValue(value, listRef);
        } catch (IOException ex) {
            throw new BlastException("Failed to parse", ex);
        }
    }

    public static String objectToJson(Object value) throws BlastException {
        if (value == null) {
            return "{}";
        }
        try {
            return getObjectMapper().writeValueAsString(value);
        } catch (JsonProcessingException ex) {
            throw new BlastException("Failed to parse", ex);
        }
    }

    public static String mapToJson(Map<String, Object> value) throws BlastException {
        if (value == null) {
            return "{}";
        }
        try {
            return getObjectMapper().writeValueAsString(value);
        } catch (JsonProcessingException ex) {
            throw new BlastException("Failed to parse", ex);
        }
    }

    public static Map<String, Object> objectToMap(Object value) {
        if (value == null) {
            return new HashMap<>();
        }
        Map<String, Object> map;
        map = getObjectMapper().convertValue(value, MAP_TYPE_REF);
        return map;
    }

    public static Map<String, Object> jsonToMap(String value) throws BlastException {
        if (value == null) {
            return new HashMap<>();
        }
        Map<String, Object> map;
        try {
            map = getObjectMapper().readValue(value, MAP_TYPE_REF);
        } catch (IOException ex) {
            throw new BlastException("Failed to parse", ex);
        }
        return map;
    }

    public static <T extends IGraphData> T objectFromMap(Map<String, Object> value, Class<T> clazz) throws BlastException {
        try {
            return getObjectMapper().readValue(getObjectMapper().writeValueAsString(value), clazz);
        } catch (IOException ex) {
            throw new BlastException("Failed to convert Object", ex);
        }
    }

    public static String validatePathAndGetCollectionName(String key, boolean requiresCollection) throws BlastException {
        PathDetails lastPathDetail = GraphDataExtractor.getLastPathDetail(key);
        if (lastPathDetail.isRoot()) {
            throw new BlastException("Access to root not allowed");
        }
        if (requiresCollection && !lastPathDetail.isCollectionOnly()) {
            throw new BlastException("Can only add to collections");
        }
        if (!requiresCollection && lastPathDetail.isCollectionOnly()) {
            throw new BlastException("Can only modify a record not a collection");
        }

        for (String internal : INTERNAL_COLLECTIONS) {
            if (internal.equals(lastPathDetail.getCollection())) {
                throw new BlastException("Invalid collection");
            }
        }

        return lastPathDetail.getCollection();

    }

    public static <T> Set<String> getFieldNames(T dataObject) {
        Set<String> properties = new HashSet<>();
        Map<String, Object> map = getObjectMapper().convertValue(dataObject, MAP_TYPE_REF);
        safeStream(map.entrySet()).forEach(entry -> {
            switch (entry.getKey()) {
                //TODO what to do with id (and possibly type) ?
                case "id":
                    break;
                default:
                    properties.add(entry.getKey());
            }
        });
        return properties;
    }

    public static String getKeyValueFromObject(Class<?> keyType, Object keyValue) throws BlastException {
        if (keyType.getCanonicalName().equals(keyValue.getClass().getCanonicalName())) {
            return keyValue.toString();
        }
        String value = keyValue.toString();
        return getKeyValue(keyType, value);
    }

    /**
     * Converts String to an Object of type specified in key type
     *
     * @param keyType  The Type to be converted to
     * @param keyValue The key value
     * @return
     * @throws BlastException
     */
    public static String getKeyValue(Class<?> keyType, String keyValue) throws BlastException {
        switch (keyType.getCanonicalName()) {
            case "java.lang.Long":
                try {
                    return Long.valueOf(keyValue).toString();
                } catch (NumberFormatException ex) {
                    throw new BlastException("Invalid key value - s/be a Long value is[" + keyValue + "]");
                }
            case "java.lang.Integer":
                try {
                    return Integer.valueOf(keyValue).toString();
                } catch (NumberFormatException ex) {
                    throw new BlastException("Invalid key value - s/be a Long value is[" + keyValue + "]");
                }
            case "java.lang.String":
                return keyValue;
        }
        throw new BlastException("Key type of [" + keyType.getCanonicalName() + "] is not supported");
    }

    public static <T> void addProperties(GuslVertex vertex, T dataObject) {
        Map<String, Object> map = getObjectMapper().convertValue(dataObject, TYPE_REF);
        safeStream(map.entrySet()).forEach(entry -> {
            switch (entry.getKey()) {
                //TODO what to do with id (and possibly type) ?
                case "id":
                    break;
                default:
                    vertex.setProperty(entry.getKey(), entry.getValue());
            }
        });
    }

    public static boolean collectionEdgeExists(GuslGraph graph, GuslVertex vertex, String edgeName, String name) {
        List<GuslEdge<? extends IGraphData>> edges = graph.getEdges(vertex, GraphDirection.OUT, edgeName);
        for (GuslEdge<? extends IGraphData> edge : edges) {
            if (name.equals(edge.getProperty(GraphCollection.COLLECTION_FIELD_NAME))) {
                return true;
            }
        }
        return false;
    }

    public static String logGraph(GuslGraph graph) {
        StringBuilder builder = new StringBuilder();
        for (GuslVertex vertex : graph.getVertices()) {
            logger.info("not implemented");
//            JSONObject jsonChild;
//            try {
//                jsonChild = GraphSONUtility.jsonFromElement(vertex, vertex.getPropertyKeys(), GraphSONMode.NORMAL);
//                builder.append("\n").append(jsonChild);
//            } catch (JSONException ex) {
//                logger.error("Failed to convert vertex", ex);
//            }
        }
        return builder.toString();

    }

    public static Map<String, Object> convertToMap(GuslVertex vertex) {
        Map<String, Object> map = safeStream(vertex.getPropertyKeys()).collect(
                Collectors.toMap(propertyKey -> propertyKey, propertyKey -> vertex.getProperty(propertyKey)));
        return map;
    }

    public static List<Field> getFieldsFor(Class<?> clazz) {
        List<Field> fields = new ArrayList<>();
        fields.addAll(Arrays.asList(clazz.getDeclaredFields()));

        Class<?> parentClass = clazz.getSuperclass();

        while (parentClass != null) {
            fields.addAll(Arrays.asList(parentClass.getDeclaredFields()));
            parentClass = parentClass.getSuperclass();
        }

        return fields;
    }

    public static String stripMethodName(String name) {
        if (name.startsWith("get")) {
            String newName = name.replace("get", "");
            return newName.substring(0, 1).toLowerCase() + newName.substring(1);
        }
        if (name.startsWith("is")) {
            String newName = name.replace("is", "");
            return newName.substring(0, 1).toLowerCase() + newName.substring(1);
        }
        return name;
    }

    public static String getCollectionNameForVertex(GuslGraph graph, GuslVertex vertex) {
        if (vertex == null) {
            return null;
        }
        List<GuslEdge<? extends IGraphData>> edges = graph.getEdges(vertex, GraphDirection.IN);
        for (GuslEdge<? extends IGraphData> edge : edges) {
            switch (edge.getLabel()) {
                case COLLECTION_EDGE_NAME:
                    return edge.getLabel();
            }
        }
        return null;
    }

    @Deprecated
    public <V> void addPropertiesOld(GuslVertex vertex, V dataObject) {
        Class<?> clazz = dataObject.getClass();
//        List<Field> fields = getFieldsFor(clazz);
//        safeStream(fields).filter((theField) -> !(theField.getName().startsWith("$"))).filter((theField) -> !(theField.getName().equals("serialVersionUID"))).forEach((theField) -> {
//
//        });
        Method[] methods = clazz.getMethods();

        safeStream(methods).filter((method) -> {
            switch (method.getName()) {
                case "toString":
                case "hashCode":
                case "equals":
                case "getClass":
                case "wait":
                case "notify":
                case "notifyAll":
                    return false;
            }
            return true;
        }).forEach(method -> {
            String modifier = Modifier.toString(method.getModifiers());
            if (modifier.contains("public")) {
                if (method.getParameterCount() == 0) {
                    Class<?> returnType = method.getReturnType();
                    boolean ignore = false;
                    switch (returnType.getCanonicalName()) {
                        case "void":
                            ignore = true;
                    }
                    if (!ignore) {
                        logger.debug("method: {} returnType: {}", method.getName(), returnType.getCanonicalName());

                        try {
                            Object value = method.invoke(dataObject);
                            String fieldName = stripMethodName(method.getName());

                            switch (fieldName) {
                                //TODO ignore reserved fields
                                case "id":
                                    break;
                                default:
                                    vertex.setProperty(fieldName, value);
                            }

                        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                            logger.warn("Failed to get information for method: {} on class: {}", method.getName(), clazz.getCanonicalName(), ex);
                        }
                    }
                }
            }

        });

    }

    public static String prettyTimeDifference(ZonedDateTime fromDateTime, ZonedDateTime toDateTime) {
        return prettyTimeDifference(fromDateTime.toLocalDateTime(), toDateTime.toLocalDateTime());
    }

    public static String prettyTimeDifference(LocalDateTime fromDateTime, LocalDateTime toDateTime) {
        LocalDateTime tempDateTime = LocalDateTime.from(fromDateTime);

        long years = tempDateTime.until(toDateTime, ChronoUnit.YEARS);
        tempDateTime = tempDateTime.plusYears(years);

        long months = tempDateTime.until(toDateTime, ChronoUnit.MONTHS);
        tempDateTime = tempDateTime.plusMonths(months);

        long days = tempDateTime.until(toDateTime, ChronoUnit.DAYS);
        tempDateTime = tempDateTime.plusDays(days);

        long hours = tempDateTime.until(toDateTime, ChronoUnit.HOURS);
        tempDateTime = tempDateTime.plusHours(hours);

        long minutes = tempDateTime.until(toDateTime, ChronoUnit.MINUTES);
        tempDateTime = tempDateTime.plusMinutes(minutes);

        long seconds = tempDateTime.until(toDateTime, ChronoUnit.SECONDS);

        long milli = tempDateTime.until(toDateTime, ChronoUnit.MILLIS);

        StringBuilder builder = new StringBuilder();
        if (years > 0) {
            builder.append(years).append(" years ");
        }
        if (months > 0) {
            builder.append(months).append(" months ");
        }
        if (days > 0) {
            builder.append(days).append(" days ");
        }
        if (hours > 0) {
            builder.append(hours).append(" hours ");
        }
        if (minutes > 0) {
            builder.append(minutes).append(" minutes ");
        }
        if (seconds > 0) {
            builder.append(seconds).append(" seconds ");
        }
        if (seconds < 1 && milli > 0) {
            builder.append(milli).append(" ms ");
        }
        return builder.toString();
    }

//    @Deprecated
//    private VertexDetails findVertex(Graph graph, String key) throws BlastException {
//        logger.info("key->{}", key);
//        if (key == null || key.trim().isEmpty()) {
//            // root
//            return new VertexDetails(graph.getVertex(COLLECTION_MASTER));
//        }
//
//        if (key.startsWith("/")) {
//            throw new BlastException("key cannot start with / - must specify collection e.g. collection/keyname:value");
//        }
//
//        String[] splits = key.split("/");
//
//        if (splits[0].contains(":")) {
//            throw new BlastException("must specify collection e.g. collection/keyname:value");
//        }
//
//        // check if it is collection only
//        if (splits.length == 1) {
//            // collection
//            return new VertexDetails(true, graph.getVertex(key), key, null);
//        }
//
//        String lastCollection = splits[0];
//        String lastKeyValue = null;
//        String[] queryParams = null;
//        for (int x = 1; x < splits.length; x++) {
//            if (splits[x].contains(":")) {
//                String[] keySplit = splits[x].split(":");
//                if (keySplit.length != 2) {
//                    throw new BlastException("look up must be in form keyname:value - found: " + splits[x]);
//                }
//                lastKeyValue = keySplit[1];
//            } else {
//                if (splits[x].startsWith("?")) {
//                    queryParams = splits[x].substring(1).split(",");
//                    lastKeyValue = null;
//                } else {
//                    lastCollection = splits[x];
//                    lastKeyValue = null;
//                }
//            }
//        }
//
//        //use array of path details i.e. collection, id ... then can reverse parse if necessary
//        logger.info("lastCollection: {} lastKeyValue: {} queryParams: {}", lastCollection, lastKeyValue, queryParams);
//
//        // 1st get collection so we know type of key
//        GraphCollection graphCollection = GraphCollection.getCollection(graph, lastCollection);
//
//        if (lastKeyValue == null) {
//            // its a collection of a parent keyname:value
//            // so need to find parent record first
//            return new VertexDetails(true, graph.getVertex(COLLECTION_PREFIX + lastCollection), key, queryParams);
//        } else {
//            Object keyValue = getKeyValue(graphCollection.getKeyType(), lastKeyValue);
//            return new VertexDetails(false, graph.getVertex(keyValue), key, null);
//        }
//
//    }
}
