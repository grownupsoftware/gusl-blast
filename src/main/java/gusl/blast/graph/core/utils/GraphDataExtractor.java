package gusl.blast.graph.core.utils;

import gusl.blast.exception.BlastException;
import gusl.blast.graph.core.GraphCollection;
import gusl.blast.graph.core.dataobjects.*;
import gusl.blast.model.IGraphData;
import gusl.blast.server.BlastServer;
import lombok.CustomLog;

import java.util.*;

import static gusl.blast.graph.core.BlastGraph.ATTACH_MASTER;
import static gusl.blast.graph.core.BlastGraph.CLIENT_MASTER;
import static gusl.blast.graph.core.GraphCollection.*;
import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@CustomLog
public class GraphDataExtractor {

    public static List<Map<String, Object>> getCollectionData(GuslGraph graph, String key, PathParameters params) throws BlastException {
        Map<String, Object> data = getData(graph, COLLECTION_PREFIX + key, true, params);
        Object object = data.get(key);
        if (object == null) {
            return new ArrayList<>();
        }
        return (List<Map<String, Object>>) object;
    }

    public static Map<String, Object> getData(GuslGraph graph, String key, PathParameters params) throws BlastException {
        return getData(graph, key, false, params);
    }

    public static Map<String, Object> getData(GuslGraph graph, GuslVertex vertex, boolean isCollection, PathParameters params) {
        boolean includeFields = true;
        if (vertex.getId().startsWith(COLLECTION_PREFIX) || vertex.getId().equals(COLLECTION_MASTER)) {
            includeFields = false;
        }

        List<Map<String, Object>> newList = new ArrayList<>();
        Map<String, Object> mainMap = getChildData(graph, vertex, includeFields, newList, params);
        if (isCollection) {
            mainMap.put(vertex.getId().replace(COLLECTION_PREFIX, ""), newList);
        }

        return mainMap;

    }

    public static Map<String, Object> getData(GuslGraph graph, String key, boolean isCollection, PathParameters params) throws BlastException {
        GuslVertex vertex = graph.getVertex(key);
        if (vertex == null) {
            throw new BlastException("No record found: " + key);
        }

        return getData(graph, vertex, isCollection, params);

    }

    private static Map<String, Object> getChildData(GuslGraph graph, GuslVertex vertex, boolean includeFields, List<Map<String, Object>> childList, PathParameters params) {
        Map<String, Object> map = new HashMap<>();
        if (vertex == null) {
            return map;
        }

        final List<GuslEdge<? extends IGraphData>> parentEdges = graph.getEdges(vertex, GraphDirection.IN, COLLECTION_DATA_EDGE);
        GuslVertex collectionVertex = null;
        Set<String> fieldNames = new HashSet<>();
        if (!parentEdges.isEmpty()) {
            collectionVertex = graph.getVertex(parentEdges.get(0).getParentId());
            fieldNames = collectionVertex.getFieldNames(COLLECTION_DATA_FIELD_NAMES);
        }
        if (nonNull(collectionVertex)) {
            if (includeFields && (params == null || (params.getFields() == null || params.getFields().isEmpty()))) {
                safeStream(fieldNames).forEach((property) -> {
                    try {
                        map.put(property, vertex.getPropertyValue(property));
                    } catch (Throwable t) {
                        logger.error("Error extracting data: {} {} {} ",vertex.getId(), property, vertex.getProperties());
                    }
                });
                // always add the key
                map.put(collectionVertex.getProperty(COLLECTION_FIELD_KEYNAME), vertex.getId());
            } else if (params != null && params.getFields() != null && !params.getFields().isEmpty()) {
                logger.debug("only include fields: {}", params.getFields());
                safeStream(params.getFields()).forEach((property) -> {
                    map.put(property, vertex.getPropertyValue(property));
                });
                // always add the key
                map.put(collectionVertex.getProperty(COLLECTION_FIELD_KEYNAME), vertex.getId());
            }
        }
        Map<String, List<Map<String, Object>>> edgeCollections = new HashMap<>();

        List<GuslEdge<? extends IGraphData>> edges = graph.getEdges(vertex, GraphDirection.OUT);

        for (GuslEdge<? extends IGraphData> edge : edges) {
            GuslVertex child = graph.getVertex(edge, GraphDirection.IN);

            //String childName = child.getId().toString();
            boolean includeChildFields = true;
            switch (edge.getLabel()) {
                case COLLECTION_EDGE_NAME:
                    // strip 'collection-' from id
                    String childName = child.getId().toString().replace(COLLECTION_PREFIX, "");
                    includeChildFields = false;
                    List<Map<String, Object>> newList = edgeCollections.get(childName);
                    if (newList == null) {
                        newList = new ArrayList<>();
                        edgeCollections.put(childName, newList);
                    }

                    // vertex.getId().equals(COLLECTION_MASTER) ||
                    if (isNull(params) || params.hasIncludeChildren()) {
                        childList.add(getChildData(graph, child, includeChildFields, newList, params));
                    } else {
                        logger.debug("ignoring collection children: {} ", edge.getLabel());
                    }
                    break;
                case COLLECTION_DATA_EDGE:
                    childList.add(getChildData(graph, child, includeChildFields, childList, params));
                    break;
                default:
                    if (params == null || params.hasIncludeChildren()) {
                        List<Map<String, Object>> edgeList = edgeCollections.get(edge.getLabel());
                        if (edgeList == null) {
                            edgeList = new ArrayList<>();
                            edgeCollections.put(edge.getLabel(), edgeList);
                        }

                        edgeList.add(getChildData(graph, child, includeChildFields, edgeList, params));
                        map.put(edge.getLabel(), edgeList);
                    } else {
                        logger.debug("ignoring child: {} ", edge.getLabel());
                    }

            }
        }
        if (!includeFields) {
            safeStream(edgeCollections.entrySet()).forEach(entry -> {
                map.put(entry.getKey(), entry.getValue());
            });
        }
        return map;
    }

    public static List<Map<String, Object>> getAllEdgeData(GuslGraph graph, GuslVertex vertex, String edgeName, PathParameters params) {
        List<Map<String, Object>> list = new ArrayList<>();
        List<GuslEdge<? extends IGraphData>> edges = graph.getEdges(vertex, GraphDirection.OUT, edgeName);
        for (GuslEdge<? extends IGraphData> edge : edges) {
            GuslVertex child = graph.getVertex(edge, GraphDirection.IN);
            list.add(getChildData(graph, child, true, list, params));

        }
        return list;
    }

    public static PathDetails getLastPathDetail(String key) throws BlastException {
        PathDetails[] pathDetails = PathDetails.splitPath(key);
        return pathDetails[pathDetails.length - 1];
    }

    public static Object fetch(BlastServer blastServer, GuslGraph graph, String key, PathParameters params) throws BlastException {

        PathDetails[] pathDetails = PathDetails.splitPath(key);
        if (pathDetails.length == 1) {
            // simple path - can only be 'root' or a 'collection'
            if (pathDetails[0].isRoot()) {
                return getData(graph, COLLECTION_MASTER, false, params);
            } else if (pathDetails[0].isCollectionOnly()) {
                return getCollectionData(graph, pathDetails[0].getCollection(), params);
            } else {
                if (pathDetails[0].getCollection().equals(CLIENT_MASTER) || pathDetails[0].getCollection().equals(ATTACH_MASTER)) {
                    // these are system collections
                    return getData(graph, pathDetails[0].getKeyValue(), false, params);
                } else {

                    if (pathDetails[0].getCollection() != null && pathDetails[0].getKeyField() != null) {
                        return getDataForId(blastServer, graph, pathDetails[0], params);
                    } else {
                        // should not get here - should be caught earlier
                        throw new BlastException("Must specify a collection");
                    }
                }
            }
        } else {
            // complex path - can be a 'collection' of a parent, or parent
            int index = pathDetails.length - 1;
            if (pathDetails[index].getKeyValue() != null) {
                // has a key, as keys are unique we can go and get data straight away
                return getDataForId(blastServer, graph, pathDetails[index], params);
            }
            // so it is a collection of a parent id e.g events/id:100/markets
            // need to get collection from parent
            return getDataForCollectionOfParent(blastServer, graph, pathDetails[index - 1], pathDetails[index].getCollection(), params);
        }
    }

    private static List<Map<String, Object>> getDataForCollectionOfParent(
            BlastServer blastServer,
            GuslGraph graph,
            PathDetails pathDetails,
            String collection,
            PathParameters params
    ) throws BlastException {

        GraphCollection graphCollection = GraphCollection.getCollection(blastServer, graph, pathDetails.getCollection());
        if (!pathDetails.getKeyField().equals(graphCollection.getKeyField())) {
            throw new BlastException("keyfield specified must match collections key field i.e. " + pathDetails.getKeyField() + " s/be " + graphCollection.getKeyField());
        }

        String parentKeyValue = GraphUtils.getKeyValue(graphCollection.getKeyType(), pathDetails.getKeyValue());
        GuslVertex parent = graph.getVertex(parentKeyValue);

        return getAllEdgeData(graph, parent, collection, params);
    }

    private static Map<String, Object> getDataForId(BlastServer blastServer, GuslGraph graph, PathDetails pathDetails, PathParameters params) throws BlastException {
        return getDataForId(blastServer, graph, pathDetails.getCollection(), pathDetails.getKeyField(), pathDetails.getKeyValue(), params);
    }

    private static Map<String, Object> getDataForId(BlastServer blastServer, GuslGraph graph, String collection, String keyField, String keyValue, PathParameters params) throws BlastException {
        GraphCollection graphCollection = GraphCollection.getCollection(blastServer, graph, collection);
        if (!keyField.equals(graphCollection.getKeyField())) {
            throw new BlastException("keyfield specified must match collections key field i.e. " + keyField + " s/be " + graphCollection.getKeyField());
        }
        String convertedKeyValue = GraphUtils.getKeyValue(graphCollection.getKeyType(), keyValue);
        return getData(graph, convertedKeyValue, false, params);
    }
}
