package gusl.blast.graph.core.utils;

public interface ModificationWatcher {

    void added(Object value);

    void changed(Object value);

    void removed(Object value);
}
