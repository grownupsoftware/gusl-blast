package gusl.blast.graph.core.utils;

import gusl.blast.exception.BlastException;
import gusl.blast.graph.core.dataobjects.*;
import gusl.blast.model.IGraphData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static gusl.blast.graph.core.GraphCollection.COLLECTION_MASTER;
import static gusl.core.json.JsonUtils.prettyPrint;
import static java.util.Objects.nonNull;

public class SchemaUtils {

    public static String dumpSchema(GuslGraph graph) throws BlastException {
        return dumpSchema(graph, COLLECTION_MASTER);
    }

    public static String dumpSchema(GuslGraph graph, String collectionName) throws BlastException {
        return (String) prettyPrint(getSchema(graph, collectionName));
    }

    public static Map<String, Object> getSchema(GuslGraph graph) throws BlastException {
        return getSchema(graph, COLLECTION_MASTER);
    }

    public static Map<String, Object> getSchema(GuslGraph graph, String collectionName) {
        boolean nameOnly = false;
        Map<String, Object> mainMap = new HashMap<>();

        GuslVertex vCollection = graph.getVertex(collectionName);
        Map<String, Object> collectionMap = new HashMap<>();

//        List<Map<String, Object>> childList = new ArrayList<>();
        List<GuslEdge<? extends IGraphData>> edges = graph.getEdges(vCollection, GraphDirection.OUT);
        for (GuslEdge<? extends IGraphData> edge : edges) {
            Map<String, Object> childMap = new HashMap<>();
            GuslVertex child = graph.getVertex(edge, GraphDirection.IN);
            String name = child.getProperty("name");
            if (nameOnly) {
                childMap.put("name", child.getProperty("name"));
            } else {
                if (nonNull(child.getPropertyKeys())) {
                    child.getPropertyKeys().forEach((property) -> childMap.put(property, child.getProperty(property)));
                }
            }
            //childList.add(childMap);
            collectionMap.put(name, childMap);
        }
//        mainMap.put(collectionName, childList);
        mainMap.put(collectionName, collectionMap);

        return mainMap;

    }
}
