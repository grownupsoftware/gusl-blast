package gusl.blast.graph.core.example.model;

import gusl.blast.model.IGraphData;
import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

import java.util.Set;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Market Information")
public class MarketDO  implements IGraphData {

    @DocField(description = "Id of market")
    private String id;

    @DocField(description = "Id of event")
    private String eventId;

    @DocField(description = "Name of market")
    private String name;

    @DocField(description = "The status of this market")
    private String status;

    @DocField(description = "The runners for this market")
    private Set<RunnerDO> runners;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
