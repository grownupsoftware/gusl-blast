package gusl.blast.graph.core.example.model;

import gusl.blast.model.IGraphData;
import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

import java.math.BigDecimal;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Price Information")
public class PriceDO  implements IGraphData {

    @DocField(description = "Id of runner")
    private String id;

    @DocField(description = "Id of runner")
    private BigDecimal price;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
