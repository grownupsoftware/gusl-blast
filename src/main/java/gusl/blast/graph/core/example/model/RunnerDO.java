package gusl.blast.graph.core.example.model;

import gusl.blast.model.IGraphData;
import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

import java.util.Set;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Market Information")
public class RunnerDO  implements IGraphData {

    @DocField(description = "Id of runner")
    private String id;

    @DocField(description = "Id of event")
    private String eventId;

    @DocField(description = "Id of market")
    private String marketId;

    @DocField(description = "Name of runner")
    private String name;

    @DocField(description = "The status of this runner")
    private String status;

    @DocField(description = "The prices for this runner")
    private Set<PriceDO> prices;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
