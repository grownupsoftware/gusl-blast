package gusl.blast.graph.core.example.model;

import gusl.blast.model.IGraphData;
import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;
import gusl.blast.model.IGraphData;

import java.util.Set;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Event Information")
public class EventDO implements IGraphData {

    @DocField(description = "Id of event")
    private String id;

    @DocField(description = "Name of event")
    private String name;

    @DocField(description = "The markets for this event")
    private Set<MarketDO> markets;

    @DocField(description = "The status of this event")
    private String status;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
