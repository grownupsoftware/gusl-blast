package gusl.blast.graph.core.example;

import gusl.blast.exception.BlastException;
import gusl.blast.graph.core.BlastGraph;
import gusl.blast.graph.core.GraphCollection;
import gusl.blast.graph.core.example.model.EventDO;
import gusl.blast.graph.core.example.model.MarketDO;
import gusl.blast.graph.core.example.model.RunnerDO;
import gusl.core.utils.IdGeneratorV1;
import lombok.*;

import java.util.concurrent.atomic.AtomicLong;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@CustomLog
public class SportsBookGraphBuilder {

    public static final Integer NUMBER_EVENTS = 2;
    public static final Integer NUMBER_MARKETS_PER_EVENT = 3;
    public static final Integer NUMBER_RUNNERS_PER_MARKET = 5;

    private final AtomicLong uniqueId = new AtomicLong(100);
    private BlastGraph theBlastGraph;

    public void buildGraph() throws BlastException {

        // Step 1 ---- create the collections
        // create events collection
        GraphCollection<EventDO, String> events = theBlastGraph.<EventDO, String>createCollection()
                .name("events")
                .keyField("id")
                .dataClass(EventDO.class)
                .excludeField("markets")
                .build();

        // create markets collection
        GraphCollection<MarketDO, String> markets = theBlastGraph.<MarketDO, String>createCollection()
                .name("markets")
                .keyField("id")
                .dataClass(MarketDO.class)
                //.onlyFields("name")
                .excludeField("runners")
                .build();

        // create runners collection
        GraphCollection<RunnerDO, String> runners = theBlastGraph.<RunnerDO, String>createCollection()
                .name("runners")
                .keyField("id")
                .dataClass(RunnerDO.class)
                //.onlyFields("name")
                .excludeField("prices")
                .build();

        // Step 2 ---- create relationships between collections
        // event 1-* markets.eventId
        markets.linkedTo(events).using("eventId");
        runners.linkedTo(markets).using("marketId");

        // Step 3 - log the schema
        //theBlastGraph.dumpSchema();
        // Step 4 - create some test data
        createData(NUMBER_EVENTS, NUMBER_MARKETS_PER_EVENT, NUMBER_RUNNERS_PER_MARKET, events, markets, runners);

    }

    private void createData(
            int numberEvents,
            int numMarketsPerEvent,
            int numRunnersPerMarket,
            GraphCollection eventCollection,
            GraphCollection marketCollection,
            GraphCollection runnerCollection
    ) throws BlastException {
        logger.info("Generating data. {} events each with {} markets each with {} runners", numberEvents, numMarketsPerEvent, numRunnersPerMarket);
        for (int eventCnt = 0; eventCnt < numberEvents; eventCnt++) {
            String eventId = getUniqueId();
            eventCollection.add(createEvent(eventId, "event " + eventId, "open"));
            for (int marketCnt = 0; marketCnt < numMarketsPerEvent; marketCnt++) {
                String marketId = getUniqueId();
                marketCollection.add(createMarket(marketId, eventId, "market " + marketId, "open"));
                for (int runnerCnt = 0; runnerCnt < numRunnersPerMarket; runnerCnt++) {
                    String runnerId = getUniqueId();
                    runnerCollection.add(createRunner(runnerId, eventId, marketId, "runner " + runnerId, "open"));
                }
            }
        }
    }

    private EventDO createEvent(String id, String name, String status) {
        EventDO eventDo = new EventDO();
        eventDo.setId(id);
        eventDo.setName(name);
        eventDo.setStatus(status);
        return eventDo;
    }

    private MarketDO createMarket(String id, String eventId, String name, String status) {
        MarketDO marketDo = new MarketDO();
        marketDo.setId(id);
        marketDo.setEventId(eventId);
        marketDo.setName(name);
        marketDo.setStatus(status);
        return marketDo;
    }

    private RunnerDO createRunner(String id, String eventId, String marketId, String name, String status) {
        RunnerDO runnerDo = new RunnerDO();
        runnerDo.setId(id);
        runnerDo.setEventId(eventId);
        runnerDo.setMarketId(marketId);
        runnerDo.setName(name);
        runnerDo.setStatus(status);
        return runnerDo;
    }

    private String getUniqueId() {
        return IdGeneratorV1.getUUID();
    }

}
