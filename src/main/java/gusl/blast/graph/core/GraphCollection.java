/*
 * Grownup Software Limited.
 */
package gusl.blast.graph.core;

import com.google.common.collect.MapDifference;
import com.google.common.collect.MapDifference.ValueDifference;
import com.google.common.collect.Maps;
import gusl.blast.client.BlastServerClient;
import gusl.blast.exception.BlastException;
import gusl.blast.graph.core.builders.RelationshipBuilder;
import gusl.blast.graph.core.dataobjects.*;
import gusl.blast.graph.core.utils.GraphDataExtractor;
import gusl.blast.graph.core.utils.GraphUtils;
import gusl.blast.model.IGraphData;
import gusl.blast.server.BlastServer;
import gusl.core.annotations.DocField;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.json.JsonUtils;
import gusl.core.utils.LambdaGUSLErrorExceptionHelper;
import gusl.model.errors.SystemErrors;
import lombok.*;

import java.lang.reflect.Field;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static gusl.blast.graph.core.dataobjects.GraphMessageType.*;
import static gusl.core.utils.LambdaGUSLErrorExceptionHelper.rethrowGUSLErrorConsumer;
import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * A collection of vertices. In essence a vertex that has an edge (relationship)
 * to its children. An example, Users is a collection of User, making it easy to
 * say 'getUsers() and retrieve all users.
 *
 * @param <T> The class of the data object
 * @param <K> The class of the key
 * @author grant
 */
@CustomLog

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class GraphCollection<T extends IGraphData, K> {

    @DocField(description = "The master vertex of all collections")
    public static final String COLLECTION_MASTER = "root";

    @DocField(description = "Collections connected to root are prefixed with this value and connected to root e.g. 'runners - > 'root")
    public static final String COLLECTION_PREFIX = "collection-";

    @DocField(description = "Edge name for collections to root e.g. 'runners' -> 'root")
    public static final String COLLECTION_EDGE_NAME = "collections";

    @DocField(description = "Edge between a data record its collection e.g. 'runner/id:1' -> 'runners'")
    public static final String COLLECTION_DATA_EDGE = "data";

    @DocField(description = "Edge between a data record and the attachment vertex e.g. 'runner/id:1 -> attachment")
    public static final String COLLECTION_ATTACHMENT_EDGE = "attachments";

    @DocField(description = "Edge between a client vertex and attachmenst e.g. 'client/clientId:abc -> attachment")
    public static final String COLLECTION_CLIENT_ATTACHMENT_EDGE = "client-attach";

    @DocField(description = "Property name for the collection name")
    public static final String COLLECTION_FIELD_NAME = "name";

    @DocField(description = "Property name for the key field that makes the record unique e.g. id")
    public static final String COLLECTION_FIELD_KEYNAME = "keyname";

    @DocField(description = "Property name for the data class name")
    public static final String COLLECTION_FIELD_CLASSNAME = "classname";

    @DocField(description = "Property name for the class of the key i.e. Integer, Long, String")
    public static final String COLLECTION_FIELD_KEYTYPE = "keytype";

    @DocField(description = "Property name to store all the field names")
    public static final String COLLECTION_DATA_FIELD_NAMES = "field-names";

    @DocField(description = "Property name for a list of parent relationships")
    public static final String COLLECTION_FIELD_PARENT_RELATIONSHIPS = "parent-relationships";

    @DocField(description = "Property name for a list of children relationships")
    public static final String COLLECTION_FIELD_CHILD_RELATIONSHIPS = "child-relationships";

    private String name;
    private String keyField;
    private GuslGraph graph;
    private Class<? extends IGraphData> dataClass;
    private Class<?> keyType;
    private Set<String> fieldNames;
    private GuslVertex collectionVertex;

    @Singular
    private List<String> includeFields;
    @Singular
    private List<String> excludeFields;

    private Map<String, Object> childRelationships;
    private Map<String, Object> parentRelationships;

    private String type;

    @DocField(description = "The associated blast server")
    private BlastServer blastServer;

    public GraphCollection(BlastServer blastServer, GuslGraph graph, GuslVertex vertex, String name, String keyName, Class<T> clazz, Class<K> keyType) {
        this.graph = graph;
        this.name = name;
        this.keyField = keyName;
        this.dataClass = clazz;
        this.keyType = keyType;
        this.collectionVertex = vertex;
        this.type = COLLECTION_DATA_EDGE;
        this.blastServer = blastServer;
    }

    public GraphCollection(BlastServer blastServer, GuslGraph graph, GuslVertex vertex, String name, String keyName, Class<T> clazz, Class<K> keyType, Map<String, Object> parentRelationships, Map<String, Object> childRelationships) {
        this(blastServer, graph, vertex, name, keyName, clazz, keyType);
        this.childRelationships = childRelationships;
        this.parentRelationships = parentRelationships;
    }

    public static class GuslGraphCollectionBuilder<T extends IGraphData, K> extends GraphCollectionBuilder<T, K> {

        private BlastGraph blastGraph;

        public GuslGraphCollectionBuilder<T, K> blastGraph(BlastGraph blastGraph) {
            this.blastGraph = blastGraph;
            return this;
        }

        @Override
        public GraphCollection<T, K> build() {
            try {
                final GraphCollection<T, K> collection = super.<T, K>build();

                if (isNull(collection.childRelationships)) {
                    collection.setChildRelationships(new HashMap<>());
                }
                if (isNull(collection.parentRelationships)) {
                    collection.setParentRelationships(new HashMap<>());
                }

                List<Field> classFields = GraphUtils.getFieldsFor(super.dataClass);
                Optional<Field> optional = safeStream(classFields)
                        .filter(field -> field.getName().equals(super.keyField))
                        .findFirst();
                if (!optional.isPresent()) {
                    throw new BlastException("Failed to find type of field [" + super.keyField + "]");
                }
                Class<String> keyType = (Class<String>) optional.get().getType();
                GraphCollection.validateKeyClass(keyType);
                collection.setKeyType(keyType);
                collection.setType(COLLECTION_EDGE_NAME);
                this.blastGraph.addCollection(collection);
                // GraphCollection graphCollection = blastGraph.createCollection(name, keyfield, dataclass, keyType, includeFields, excludeFields);
                return collection;
            } catch (BlastException e) {
                logger.error("Failed to build collection", e);
                return null;
            }

        }
    }

    /**
     * Initialise the vertex property map using a list of fields
     *
     * @param vertex        the collection vertex
     * @param includeFields the fields to include
     * @throws BlastException
     */
    public void initialiseWithFields(GuslVertex vertex, List<String> includeFields) throws BlastException {
        fieldNames = safeStream(includeFields).collect(Collectors.toSet());
        if (!fieldNames.isEmpty()) {
            fieldNames.remove("id");
            vertex.setProperty(COLLECTION_DATA_FIELD_NAMES, fieldNames);
        }
        if (parentRelationships != null && !parentRelationships.isEmpty()) {
            vertex.setProperty(COLLECTION_FIELD_PARENT_RELATIONSHIPS, GraphUtils.mapToJson(parentRelationships));
        }
        if (childRelationships != null && !childRelationships.isEmpty()) {
            vertex.setProperty(COLLECTION_FIELD_CHILD_RELATIONSHIPS, GraphUtils.mapToJson(childRelationships));
        }
    }

    public void initialiseFromClass(GuslVertex vertex, List<String> excludeFields) throws BlastException {
        try {
            T dataObject = (T) dataClass.newInstance();
            fieldNames = GraphUtils.getFieldNames(dataObject);
            safeStream(excludeFields).forEach(field -> {
                fieldNames.remove(field);
            });
            if (!fieldNames.isEmpty()) {
                vertex.setProperty(COLLECTION_DATA_FIELD_NAMES, fieldNames);
            }
            if (parentRelationships != null && !parentRelationships.isEmpty()) {
                vertex.setProperty(COLLECTION_FIELD_PARENT_RELATIONSHIPS, GraphUtils.mapToJson(parentRelationships));
            }
            if (childRelationships != null && !childRelationships.isEmpty()) {
                vertex.setProperty(COLLECTION_FIELD_CHILD_RELATIONSHIPS, GraphUtils.mapToJson(childRelationships));
            }

        } catch (InstantiationException | IllegalAccessException ex) {
            throw new BlastException("Failed to create instance of class [" + dataClass.getCanonicalName() + "] - does it have a default constructor?");
        }
    }

    public void addLink(BlastServer blastServer, Relationship relationship) throws BlastException {
        GuslVertex parent = graph.getVertex(COLLECTION_PREFIX + relationship.getParentCollection().getName());
        if (isNull(parent)) {
            throw new BlastException("Cannot find parent collection for: " + COLLECTION_PREFIX + relationship.getParentCollection().getName());
        }
        if (parentRelationships == null) {
            parentRelationships = new HashMap<>();
        }
        parentRelationships.put(this.getName(), relationship.getFieldName());
        collectionVertex.setProperty(COLLECTION_FIELD_PARENT_RELATIONSHIPS, GraphUtils.mapToJson(parentRelationships));

        // update the parent
        GraphCollection parentCollection = getCollection(blastServer, relationship.getParentCollection().getName());
        parentCollection.addChildRelationship(this.getName(), relationship.getFieldName());

    }

    /**
     * On the parent vertex add the relationship to its collection of
     * relationships
     *
     * @param collectionName the child collection name
     * @param childFieldName the field that is the link between the two
     * @throws BlastException
     */
    private void addChildRelationship(String collectionName, String childFieldName) throws BlastException {
        if (childRelationships == null) {
            childRelationships = new HashMap<>();
        }
        childRelationships.put(collectionName, childFieldName);
        collectionVertex.setProperty(COLLECTION_FIELD_CHILD_RELATIONSHIPS, GraphUtils.mapToJson(childRelationships));
    }

    /**
     * Get a GraphCollection
     *
     * @param blastServer    Associated blast server
     * @param collectionName The name of the collection to get
     * @return
     * @throws BlastException
     */
    public GraphCollection getCollection(BlastServer blastServer, String collectionName) throws BlastException {
        return getCollection(blastServer, graph, collectionName);
    }

    /**
     * statically get a Graph Collection
     *
     * @param blastServer    associated blast server
     * @param graph          The Tinkerpop graph that has this collection
     * @param collectionName the collection name
     * @return
     * @throws BlastException
     */
    public static GraphCollection getCollection(BlastServer blastServer, GuslGraph graph, String collectionName) throws BlastException {

        GuslVertex vertex = graph.getVertex(COLLECTION_PREFIX + collectionName);

        if (vertex == null) {
            throw new BlastException("Failed to find collection: " + collectionName);
        }

        String classname = vertex.getProperty(COLLECTION_FIELD_CLASSNAME);
        if (classname == null) {
            throw new BlastException("Associated data class is null");
        }
        Class<?> clazz;
        try {
            clazz = Class.forName(classname);
        } catch (ClassNotFoundException ex) {
            throw new BlastException("Failed to find class: " + classname, ex);
        }

        String keyType = vertex.getProperty(COLLECTION_FIELD_KEYTYPE);
        if (keyType == null) {
            throw new BlastException("Associated data class is null");
        }
        Class<?> keyClass;
        try {
            keyClass = Class.forName(keyType);
        } catch (ClassNotFoundException ex) {
            throw new BlastException("Failed to find class: " + classname, ex);
        }

        String name = vertex.getProperty(COLLECTION_FIELD_NAME);
        String keyname = vertex.getProperty(COLLECTION_FIELD_KEYNAME);
        Map<String, Object> parentRelationShips = GraphUtils.jsonToMap(vertex.getProperty(COLLECTION_FIELD_PARENT_RELATIONSHIPS));
        Map<String, Object> childRelationShips = GraphUtils.jsonToMap(vertex.getProperty(COLLECTION_FIELD_CHILD_RELATIONSHIPS));

        GraphCollection graphCollection = new GraphCollection(blastServer, graph, vertex, name, keyname, clazz, keyClass, parentRelationShips, childRelationShips);
        Set<String> fieldnames = vertex.getFieldNames(COLLECTION_DATA_FIELD_NAMES);
        graphCollection.initialiseWithFields(vertex, new ArrayList<>(fieldnames.size()));

        return graphCollection;

    }

    /**
     * Build a relationship - starts with child collection
     *
     * @param parentCollection
     * @return
     * @throws gusl.blast.exception.BlastException
     */
    public RelationshipBuilder linkedTo(GraphCollection parentCollection) throws BlastException {
//        if (theBlastServer == null) {
//            throw new BlastException("No Blast Server - please set the server on the graph");
//        }

        return RelationshipBuilder.builder().blastServer(blastServer).parentCollection(parentCollection).childCollection(this).build();
    }

    public List<? extends IGraphData> getAll(PathParameters params) throws GUSLErrorException {
        try {
            return safeStream(GraphDataExtractor.getCollectionData(this.graph, this.name, params))
                    .map(LambdaGUSLErrorExceptionHelper.rethrowGUSLErrorFunction(record -> GraphUtils.objectFromMap(record, this.dataClass)))
                    .collect(Collectors.toList());
        } catch (BlastException e) {
            throw SystemErrors.INVALID_CACHE_DATA.generateException(e.getMessage());
        }
    }

    public void add(List<T> records) throws BlastException {
        safeStream(records).forEach(rethrowGUSLErrorConsumer(record -> add(record)));
    }

    /**
     * Add an entity to the collection
     *
     * @param dataObject the data POJO
     * @throws BlastException
     */
    public void add(T dataObject) throws BlastException {
        if (isNull(blastServer)) {
            throw new BlastException("No Blast Server - please set the server on the graph");
        }
        Map<String, Object> map = GraphUtils.objectToMap(dataObject);

        String keyValue = (String) map.get(keyField);

        if (keyValue == null) {
            throw new BlastException("No key field found");
        }

        GuslVertex currentRecord;
        try {
            currentRecord = graph.addVertex(keyValue);
        } catch (IllegalArgumentException ex) {
            throw new BlastException("Failed to add", ex);
        }
        safeStream(fieldNames).forEach(fieldname -> {
            Object value = map.get(fieldname);
            if (value != null) {
                try {
                    currentRecord.setProperty(fieldname, value);
                } catch (IllegalArgumentException ex) {
                    logger.warn("Failed to set field name: {} [Note: some implementations do not like empty collections] - error {} ", fieldname, ex.getMessage());
                }
            }
        });

        // create the link to parent e.g. entity->user
        graph.addEdge(null, collectionVertex, currentRecord, COLLECTION_DATA_EDGE);

        // check if any parent relationships and add
        safeStream(parentRelationships.entrySet()).forEach(entry -> {

            try {
                String relationshipName = entry.getKey();

                GraphCollection parentCollection = getCollection(blastServer, relationshipName);

                // key field on child that links to parent
                String value = GraphUtils.getKeyValueFromObject(parentCollection.getKeyType(), map.get((String) entry.getValue()));
                GuslVertex parentVertex = graph.getVertex(value);

                logger.debug("=== adding child-id: {} => parent-id: {}[{}] on relation-ship-name: {}", keyValue, entry.getValue(), value, relationshipName);
                graph.addEdge(null, parentVertex, currentRecord, relationshipName);

            } catch (BlastException ex) {
                Logger.getLogger(GraphCollection.class.getName()).log(Level.SEVERE, null, ex);
            }

        });

        Map<String, Object> newRecord = GraphDataExtractor.getData(graph, currentRecord, false, null);
        logger.debug("adding record: {}", newRecord);

        StringBuilder keyBuilder = new StringBuilder();
        List<Instruction> listInstructions = new ArrayList<>();

        updateRootAttachments(currentRecord, Operation.ADD, null, newRecord, listInstructions);

        bubbleUp(null, currentRecord, Operation.ADD, listInstructions, null, keyBuilder, newRecord);
        logger.debug("add instructions: {}", JsonUtils.prettyPrint(listInstructions));
    }

    /**
     * Update an entity in the collection - if not in the collection it will add
     *
     * @param dataObject
     * @throws gusl.blast.exception.BlastException
     */
    public void update(T dataObject) throws BlastException {
        Map<String, Object> newValues = getValidFieldMap(GraphUtils.objectToMap(dataObject));
        String keyValue = (String) newValues.get(keyField);

        GuslVertex currentRecord = graph.getVertex(keyValue);

        Map<String, Object> oldValues = getValidFieldMap(convertToMap(currentRecord));

        MapDifference<String, Object> mapDifference = Maps.difference(
                newValues, oldValues);

        Map<String, ValueDifference<Object>> sameKeyDifferentValue = mapDifference
                .entriesDiffering();

        List<Map<String, Object>> listOfChanges = new ArrayList<>();
        safeStream(sameKeyDifferentValue.entrySet()).forEach(entry -> {
            if (nonNull(entry.getValue())) {
                // 1st update the record
                currentRecord.setProperty(entry.getKey(), entry.getValue().leftValue());

                Map<String, Object> change = new HashMap<>();
                change.put("name", entry.getKey());
                change.put("value", entry.getValue().leftValue());
                change.put("old", entry.getValue().rightValue());
                listOfChanges.add(change);
            } else {
                logger.info("Entry value is null {}", entry);
            }
        });

        if (listOfChanges.isEmpty()) {
            logger.debug("There are no changes - not updating");
        } else {
            logger.debug("listOfChanges: {}", listOfChanges);

            List<Instruction> listInstructions = new ArrayList<>();

            updateRootAttachments(currentRecord, Operation.UPDATE, listOfChanges, null, listInstructions);

            StringBuilder keyBuilder = new StringBuilder();
            bubbleUp(null, currentRecord, Operation.UPDATE, listInstructions, listOfChanges, keyBuilder, null);

            logger.debug("Update instructions: {}", JsonUtils.prettyPrint(listInstructions));
        }
    }

    public void remove(String keyValue) throws BlastException {
        logger.debug("Removing: {}", keyValue);
        String key = GraphUtils.getKeyValue(keyType, keyValue);

        GuslVertex currentRecord = graph.getVertex(key);

        // need to inform all that this will be removed
        // delete the vertex - this should remove all edges etc
        logger.debug("Removing: {}", currentRecord);

        StringBuilder keyBuilder = new StringBuilder();
        List<Instruction> listInstructions = new ArrayList<>();

        // bubble up, informing all
        bubbleUp(null, currentRecord, Operation.REMOVE, listInstructions, null, keyBuilder, null);
        logger.debug("remove instructions: {}", listInstructions);

        // we have to manually remove client-attachments i.e. we have an attachment vertex not connected to any record
        List<GuslEdge<? extends IGraphData>> edges = graph.getEdges(currentRecord, GraphDirection.OUT, COLLECTION_ATTACHMENT_EDGE);
        for (GuslEdge<? extends IGraphData> edge : edges) {
            GuslVertex attachVertex = graph.getVertex(edge, GraphDirection.IN);
            graph.removeVertex(attachVertex);
        }

        graph.removeVertex(currentRecord);
    }

    private void updateRootAttachments(GuslVertex currentRecord, Operation operation, List<Map<String, Object>> listOfChanges, Map<String, Object> newRecord, List<Instruction> listInstructions) throws BlastException {
        // need to update attachmenst on root        
        GuslVertex master = graph.getVertex(COLLECTION_MASTER);

        List<GuslEdge<? extends IGraphData>> edges = graph.getEdges(currentRecord, GraphDirection.IN, COLLECTION_DATA_EDGE);
        String pathCollectionName = "";
        for (GuslEdge<? extends IGraphData> edge : edges) {
            GuslVertex v = graph.getVertex(edge, GraphDirection.OUT);
            pathCollectionName = ((String) v.getId()).replace("collection-", "");
        }

        // FIXME s/be key field not id
        String path = pathCollectionName + "/" + "id" + ":" + currentRecord.getId().toString();

        edges = graph.getEdges(master, GraphDirection.OUT, COLLECTION_ATTACHMENT_EDGE);
        for (GuslEdge<? extends IGraphData> edge : edges) {
            GuslVertex attachVertex = graph.getVertex(edge, GraphDirection.IN);
            logger.debug("bubble up edge: {} parent: {}", edge.getLabel(), attachVertex.getId());
            Instruction instruction = new Instruction(operation);

            instruction.setPath(path);
            instruction.setCollection("root");

            switch (operation) {
                case ADD:
                    instruction.setRecord(newRecord);
                    break;
                case UPDATE:
                    instruction.setChanges(listOfChanges);
                    break;
                case REMOVE:
                    break;
                default:
                    throw new AssertionError(operation.name());

            }
            listInstructions.add(instruction);
            sendData(attachVertex, instruction);
        }
    }

    private void sendData(GuslVertex attachVertex, Instruction instruction) throws BlastException {

        String attachmentId = attachVertex.getProperty("attachmentId");
        String key = attachVertex.getProperty("key");

        boolean haveParams = attachVertex.haveProperty("have-params");
        boolean includeChildren = true;
        List<String> includeFields = null;
        List<QueryParameter> queryParameters = null;

        if (haveParams) {
            includeChildren = attachVertex.haveProperty("include-children");

            String flds = attachVertex.getProperty("include-fields");
            if (flds != null) {
                includeFields = GraphUtils.jsonToList(flds, String.class);
            }

            String preds = attachVertex.getProperty("predicates");
            if (preds != null) {
                queryParameters = GraphUtils.jsonToList(preds, QueryParameter.class);
            }
        }

        //logger.info("--------------------- haveParams: {} include-Children:{}", haveParams, includeChildren);
        GraphResponseMessage graphMessage;
        switch (instruction.getOperation()) {
            case ADD:
                graphMessage = new GraphResponseMessage(GRAPH_ADD_RESPONSE, attachmentId, key, instruction);
                break;
            case UPDATE:
                graphMessage = new GraphResponseMessage(GRAPH_UPDATE_RESPONSE, attachmentId, key, instruction);
                break;
            case REMOVE:
                graphMessage = new GraphResponseMessage(GRAPH_REMOVE_RESPONSE, attachmentId, key, instruction);
                break;
            default:
                throw new AssertionError(instruction.getOperation().name());

        }
        logger.debug("send graphMessage: {} ", graphMessage);

        List<GuslEdge<? extends IGraphData>> edges = graph.getEdges(attachVertex, GraphDirection.IN, COLLECTION_CLIENT_ATTACHMENT_EDGE);
        for (GuslEdge<? extends IGraphData> edge : edges) {
            GuslVertex blastClient = graph.getVertex(edge, GraphDirection.OUT);
            sendMessageToClient((String) blastClient.getId(), graphMessage);
        }
    }

    private void sendMessageToClient(String clientId, GraphResponseMessage graphMessage) throws BlastException {
        if (blastServer == null) {
            throw new BlastException("No Blast Server - please set the server on the graph");
        }
        BlastServerClient blastServerClient = blastServer.getClientByID(clientId);
        if (blastServerClient == null) {
            logger.warn("Client not found for id: {}", clientId);
            // mmm lets see who is connected
            safeStream(blastServer.getClients()).forEach(client -> {
                logger.info("Client connected: {}", client.getClientID());
            });
            return;
        }
        logger.debug("sending message to: {} message - command: {} key: {}", clientId, graphMessage.getCmd(), graphMessage.getKey());
        blastServerClient.queueMessage(graphMessage);
    }

    private void bubbleUp(String path,
                          GuslVertex vertex,
                          Operation operation,
                          List<Instruction> listInstructions,
                          List<Map<String, Object>> listOfChanges,
                          StringBuilder keyBuilder,
                          Map<String, Object> newRecord) throws BlastException {

        logger.debug("bubbleup - touched: {}", vertex.getId());
        Instruction instruction = new Instruction(operation);
        if (path == null) {
            path = "";
        }
        List<GuslEdge<? extends IGraphData>> edges = graph.getEdges(vertex, GraphDirection.IN, COLLECTION_DATA_EDGE);
        String pathCollectionName = "";
        for (GuslEdge<? extends IGraphData> edge : edges) {
            GuslVertex v = graph.getVertex(edge, GraphDirection.OUT);
            pathCollectionName = ((String) v.getId()).replace("collection-", "");
        }

        // FIXME s/be key field not id
        path = pathCollectionName + "/" + "id" + ":" + vertex.getId().toString() + (path.isEmpty() ? "" : ("/" + path));
        keyBuilder.insert(0, path);

        instruction.setPath(path);
        switch (operation) {
            case ADD:
                instruction.setRecord(newRecord);
                break;
            case UPDATE:
                instruction.setChanges(listOfChanges);
                break;
            case REMOVE:
                break;
            default:
                throw new AssertionError(operation.name());

        }
        listInstructions.add(instruction);

        edges = graph.getEdges(vertex, GraphDirection.OUT, COLLECTION_ATTACHMENT_EDGE);
        for (GuslEdge<? extends IGraphData> edge : edges) {
            GuslVertex attachVertex = graph.getVertex(edge, GraphDirection.IN);
            sendData(attachVertex, instruction);
        }

        edges = graph.getEdges(vertex, GraphDirection.IN);
        for (GuslEdge<? extends IGraphData> edge : edges) {
            if (nonNull(edge.getLabel())) {

                switch (edge.getLabel()) {
                    case COLLECTION_EDGE_NAME:
                    case COLLECTION_DATA_EDGE:
                        GuslVertex collectonVertex = graph.getVertex(edge, GraphDirection.OUT);
                        Instruction topLevelCollectionInstruction = new Instruction(operation);
                        topLevelCollectionInstruction.setCollection(((String) collectonVertex.getId()).replace("collection-", ""));
                        topLevelCollectionInstruction.setChanges(listOfChanges);
                        topLevelCollectionInstruction.setPath(path);
                        topLevelCollectionInstruction.setRecord(newRecord);
                        listInstructions.add(topLevelCollectionInstruction);

                        updateCollectionAttachments(collectonVertex, topLevelCollectionInstruction);

                        break;
                    default:
                        GuslVertex parent = graph.getVertex(edge, GraphDirection.OUT);
                        logger.debug("bubble up edge: {} parent: {}", edge.getLabel(), parent.getId());
                        bubbleUp(path, parent, operation, listInstructions, listOfChanges, keyBuilder, newRecord);
                }
            } else {
                GuslVertex parent = graph.getVertex(edge, GraphDirection.OUT);
                logger.debug("bubble up edge: {} parent: {}", edge.getLabel(), parent.getId());
                bubbleUp(path, parent, operation, listInstructions, listOfChanges, keyBuilder, newRecord);
            }
        }
    }

    private void updateCollectionAttachments(GuslVertex collectonVertex, Instruction instruction) throws BlastException {
        logger.debug("Informing attachments on collection: {}", collectonVertex.getId());
        List<GuslEdge<? extends IGraphData>> edges = graph.getEdges(collectonVertex, GraphDirection.OUT, COLLECTION_ATTACHMENT_EDGE);
        for (GuslEdge<? extends IGraphData> edge : edges) {
            GuslVertex attachVertex = graph.getVertex(edge, GraphDirection.IN);
            logger.debug("update collection attachments parent: {} edge: {} attachId: {}", collectonVertex.getId(), edge.getLabel(), attachVertex.getId());
            sendData(attachVertex, instruction);
        }
    }

    public static <K, V> Map<K, V> mapDifference(Map<? extends K, ? extends V> left, Map<? extends K, ? extends V> right) {
        Map<K, V> difference = new HashMap<>();
        difference.putAll(left);
        difference.putAll(right);
        difference.entrySet().removeAll(right.entrySet());
        return difference;
    }

    private Map<String, Object> convertToMap(GuslVertex vertex) {
        if (isNull(vertex)) {
            return new HashMap<>();
        }
        Map<String, Object> map = safeStream(vertex.getPropertyKeys()).collect(
                Collectors.toMap(propertyKey -> propertyKey, propertyKey -> vertex.getProperty(propertyKey)));
        map.put(keyField, vertex.getId());
        return map;
    }

    public static void validateKeyClass(Class<?> keyType) throws BlastException {
        switch (keyType.getCanonicalName()) {
            case "java.lang.Long":
            case "java.lang.Integer":
            case "java.lang.String":
                // valid key types
                //TODO - check more keytypes should be allowed
                return;
        }
        throw new BlastException("Key type of [" + keyType.getCanonicalName() + "] is not supported");
    }

    @Override
    public String toString() {
        return "GraphCollection{" + "name=" + name + ", keyName=" + keyField + ", dataClass=" + (isNull(dataClass) ? "null" : dataClass.getCanonicalName()) + ", keyType=" + keyType.getCanonicalName() + ", fieldNames=" + fieldNames + ", type=" + type + ", childRelationships=" + childRelationships + ", parentRelationships=" + parentRelationships + '}';
    }

    public Map<String, Object> getValidFieldMap(Map<String, Object> currentMap) {
        Map<String, Object> newMap = new HashMap<>();
        safeStream(fieldNames).forEach(fieldname -> {
            newMap.put(fieldname, currentMap.get(fieldname));
        });
        // add the key 
        newMap.put(keyField, currentMap.get(keyField));
        return newMap;
    }

}
