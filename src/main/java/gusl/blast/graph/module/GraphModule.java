/*
 * Grownup Software Limited.
 */
package gusl.blast.graph.module;

import gusl.blast.Controllable;
import gusl.blast.command.CommandModule;
import gusl.blast.exception.BlastException;
import gusl.blast.graph.core.BlastGraph;
import gusl.blast.graph.core.commands.GraphCommand;
import gusl.blast.graph.core.events.*;
import gusl.blast.module.BlastModule;
import gusl.blast.server.BlastServer;
import lombok.CustomLog;

@CustomLog
public class GraphModule implements BlastModule, Controllable {

    public static final String GRAPH_MODULE_ID = "co.gusl.graph";

    private GraphCommandHandler theGraphCommandHandler;

    private BlastGraph theBlastGraph;

    private static boolean configured = false;

    public GraphModule(BlastGraph blastGraph) {
        this.theBlastGraph = blastGraph;
    }

    @Override
    public void configure(BlastServer server) throws BlastException {
        if (!configured) {
            configured = true;

            logger.info("-- configure Graph Module --");

            theBlastGraph.setBlastServer(server);

            CommandModule commandModule = (CommandModule) server.requireModule(CommandModule.COMMAND_DECODER_MODULE_ID, CommandModule.class);

            commandModule.registerCommand("attach", AttachEvent.class, GraphCommand.class);
            commandModule.registerCommand("detach", DetachEvent.class, GraphCommand.class);
            commandModule.registerCommand("detachAll", DetachAllEvent.class, GraphCommand.class);
            commandModule.registerCommand("fetch", FetchEvent.class, GraphCommand.class);
            commandModule.registerCommand("add", AddEvent.class, GraphCommand.class);
            commandModule.registerCommand("update", UpdateEvent.class, GraphCommand.class);
            commandModule.registerCommand("remove", RemoveEvent.class, GraphCommand.class);
            commandModule.registerCommand("schema", SchemaEvent.class, GraphCommand.class);
            commandModule.registerCommand("attachments", AttachmentsEvent.class, GraphCommand.class);

            // Add Graph to the Eventbus
            theGraphCommandHandler = new GraphCommandHandler(theBlastGraph);
            server.registerEventListener(theGraphCommandHandler);
        }
    }

    public void setGraph(BlastGraph theBlastGraph) {
        this.theBlastGraph = theBlastGraph;
    }

    public BlastGraph getGraph() {
        return theBlastGraph;
    }

    @Override
    public String getModuleID() {
        return GRAPH_MODULE_ID;
    }

    @Override
    public void startup() throws BlastException {
    }

    @Override
    public void shutdown() {
    }

}
